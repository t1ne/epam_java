package com.epam.events.tickets.booking.model.impl;

import com.epam.events.tickets.booking.model.Ticket;
import lombok.Data;

@Data
public class TicketImpl implements Ticket {
    private long id;
    private long eventId;
    private long userId;
    private Category category;
    private int place;
}
