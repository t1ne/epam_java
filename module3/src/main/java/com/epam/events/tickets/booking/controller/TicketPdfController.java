package com.epam.events.tickets.booking.controller;

import com.epam.events.tickets.booking.facade.BookingFacade;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.List;

@Controller
public class TicketPdfController {

    private final BookingFacade bookingFacade;

    public TicketPdfController(BookingFacade bookingFacade) {
        this.bookingFacade = bookingFacade;
    }

    @GetMapping(value = "/tickets/byUser/{userId}", headers = "Accept=application/pdf", produces = "application/pdf")
    public ResponseEntity<InputStreamResource> getBookedTicketsByUser(@PathVariable("userId") Long userId,
                                                                      @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNum,
                                                                      @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize) {
        User user = new UserImpl();
        user.setId(userId);
        List<Ticket> tickets = bookingFacade.getBookedTickets(user, pageSize, pageNum);
        ByteArrayOutputStream out = getTicketsOutputStream(tickets);

        return ResponseEntity
                .ok()
                .header("Content-Disposition", "inline; filename=tickets.pdf")
                .contentType(MediaType.APPLICATION_PDF)
                .body(new InputStreamResource(new ByteArrayInputStream(out.toByteArray())));
    }

    private ByteArrayOutputStream getTicketsOutputStream(List<Ticket> tickets) {
        Document document = new Document();
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(document, out);
            document.open();
            Font font = FontFactory.getFont(FontFactory.COURIER, 14, BaseColor.BLACK);
            for (Ticket ticket : tickets) {
                Paragraph paragraph = new Paragraph(ticket.toString(), font);
                document.add(paragraph);
            }
            document.close();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
        return out;
    }
}
