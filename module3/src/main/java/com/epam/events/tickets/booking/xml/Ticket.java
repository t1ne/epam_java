package com.epam.events.tickets.booking.xml;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Root(name="ticket")
public class Ticket {
    @Attribute(name="event")
    private long eventId;
    @Attribute(name="user")
    private long userId;
    @Attribute(name="category")
    private String category;
    @Attribute(name="place")
    private int place;
}
