package com.epam.events.tickets.booking.parser;

import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.UserImpl;

import java.util.List;

public class UserParser {

    private static final int NAME_INDEX = 0;
    private static final int EMAIL_INDEX = 1;

    public static User parse(long id, List<String> objectData) {
        User user = new UserImpl();
        user.setId(id);
        user.setName(objectData.get(NAME_INDEX));
        user.setEmail(objectData.get(EMAIL_INDEX));
        return user;
    }
}
