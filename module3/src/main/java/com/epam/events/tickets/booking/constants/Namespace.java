package com.epam.events.tickets.booking.constants;

public class Namespace {
    private Namespace() {}

    public static final String EVENT = "event";
    public static final String USER = "user";
    public static final String TICKET = "ticket";
}
