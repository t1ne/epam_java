package com.epam.events.tickets.booking.service;

import com.epam.events.tickets.booking.dao.Dao;
import com.epam.events.tickets.booking.exception.UserNotFoundException;
import com.epam.events.tickets.booking.model.User;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

public class UserService {

    @Setter
    private Dao<User> userDao;

    public User getUserById(long userId) throws UserNotFoundException {
        return userDao.get(userId).orElseThrow(UserNotFoundException::new);
    }

    public User getUserByEmail(String email) throws UserNotFoundException {
        return userDao.getAll().stream()
                .filter(user -> user.getEmail().equals(email))
                .findAny()
                .orElseThrow(UserNotFoundException::new);
    }

    public List<User> getUsersByName(String name, int pageSize, int pageNum) {
        return userDao.getAll().stream()
                .filter(user -> user.getName().contains(name))
                .skip((long) (pageNum - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public User createUser(User user) {
        return userDao.save(user);
    }

    public User updateUser(User user) {
        return userDao.update(user);
    }

    public boolean deleteUser(long userId) {
        return userDao.delete(userId);
    }
}
