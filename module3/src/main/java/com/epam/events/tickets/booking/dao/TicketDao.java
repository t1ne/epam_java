package com.epam.events.tickets.booking.dao;

import com.epam.events.tickets.booking.constants.Deliminator;
import com.epam.events.tickets.booking.constants.Namespace;
import com.epam.events.tickets.booking.model.Ticket;
import lombok.Setter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class TicketDao implements Dao<Ticket> {

    @Setter
    private Storage storage;

    @Override
    public Optional<Ticket> get(long id) {
        return Optional.ofNullable((Ticket) storage.get(Namespace.TICKET + Deliminator.COLON + id));
    }

    @Override
    public List<Ticket> getAll() {
        return storage
                .getAllByNamespace(Namespace.TICKET)
                .stream()
                .map(storageEntry -> (Ticket) storageEntry)
                .collect(Collectors.toList());
    }

    @Override
    public Ticket save(Ticket ticket) {
        long newId = storage.getNextFreeIdForNamespace(Namespace.TICKET);
        ticket.setId(newId);
        storage.put(Namespace.TICKET + Deliminator.COLON + newId, ticket);
        return ticket;
    }

    @Override
    public Ticket update(Ticket ticket) {
        storage.put(Namespace.TICKET + Deliminator.COLON + ticket.getId(), ticket);
        return ticket;
    }

    @Override
    public boolean delete(long id) {
        return storage.remove(Namespace.TICKET + Deliminator.COLON + id);
    }
}
