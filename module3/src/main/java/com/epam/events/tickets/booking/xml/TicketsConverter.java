package com.epam.events.tickets.booking.xml;

import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.impl.TicketImpl;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class TicketsConverter {

    public static List<Ticket> parseTickets(InputStream inputStream){
        Serializer ser = new Persister();
        TicketListContainer tickets;
        try {
            tickets = ser.read(TicketListContainer.class, inputStream);
            return convertFromDtoToModel(tickets.getList());
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }

    private static List<Ticket> convertFromDtoToModel(List<com.epam.events.tickets.booking.xml.Ticket> dtoTickets) {
        return dtoTickets
                .stream()
                .map(dtoTicket -> {
                    Ticket plainTicket = new TicketImpl();
                    plainTicket.setUserId(dtoTicket.getUserId());
                    plainTicket.setEventId(dtoTicket.getEventId());
                    plainTicket.setCategory(Ticket.Category.valueOf(dtoTicket.getCategory()));
                    plainTicket.setPlace(dtoTicket.getPlace());
                    return plainTicket;
                })
                .collect(Collectors.toList());
    }
}
