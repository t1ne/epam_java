package com.epam.events.tickets.booking.controller;

import com.epam.events.tickets.booking.exception.EventNotFoundException;
import com.epam.events.tickets.booking.facade.BookingFacade;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.impl.EventImpl;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/events")
public class EventController {

    private final BookingFacade bookingFacade;

    public EventController(BookingFacade bookingFacade) {
        this.bookingFacade = bookingFacade;
    }

    @GetMapping
    public ModelAndView getIndexPage() {
        ModelAndView mav = new ModelAndView("events");
        mav.addObject("events", Collections.emptyList());
        return mav;
    }

    @GetMapping("/{id}")
    public ModelAndView getEventById(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("events");
        mav.addObject("events", List.of(bookingFacade.getEventById(id)));
        return mav;
    }

    @GetMapping("/byTitle/{title}")
    public ModelAndView getEventsByTitle(@PathVariable("title") String title,
                                      @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNum,
                                      @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize) {
        ModelAndView mav = new ModelAndView("events");
        mav.addObject("events", bookingFacade.getEventsByTitle(title, pageSize, pageNum));
        return mav;
    }

    @GetMapping("/byDay/{day}")
    public ModelAndView getEventsForDay(@PathVariable("day") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date day,
                                         @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNum,
                                         @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize) {
        ModelAndView mav = new ModelAndView("events");
        mav.addObject("events", bookingFacade.getEventsForDay(day, pageSize, pageNum));
        return mav;
    }

    @PostMapping
    public ModelAndView createEvent(@RequestParam(name = "title") String title,
                                     @RequestParam(name = "date")
                                     @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date date) {
        ModelAndView mav = new ModelAndView("events");
        Event event = new EventImpl();
        event.setTitle(title);
        event.setDate(date);
        mav.addObject("events", List.of(bookingFacade.createEvent(event)));
        return mav;
    }

    @PutMapping("/{id}")
    public ModelAndView updateEvent(@PathVariable("id") Long id,
                                   @RequestParam(name = "title") String title,
                                   @RequestParam(name = "date")
                                   @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)Date date) {
        ModelAndView mav = new ModelAndView("events");
        Event event = new EventImpl();
        event.setId(id);
        event.setTitle(title);
        event.setDate(date);
        mav.addObject("events", List.of(bookingFacade.updateEvent(event)));
        return mav;
    }

    @DeleteMapping("/{id}")
    public ModelAndView deleteEvent(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("events");
        mav.addObject("isUserDeleted", bookingFacade.deleteEvent(id));
        return mav;
    }
}
