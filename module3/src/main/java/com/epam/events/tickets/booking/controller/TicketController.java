package com.epam.events.tickets.booking.controller;

import com.epam.events.tickets.booking.facade.BookingFacade;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.EventImpl;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/tickets")
public class TicketController {

    private final BookingFacade bookingFacade;

    public TicketController(BookingFacade bookingFacade) {
        this.bookingFacade = bookingFacade;
    }

    @GetMapping
    public ModelAndView getIndexPage() {
        ModelAndView mav = new ModelAndView("tickets");
        mav.addObject("tickets", Collections.emptyList());
        return mav;
    }

    @PostMapping
    public ModelAndView bookTicket(@RequestParam(name = "userId") Long userId,
                                    @RequestParam(name = "eventId") Long eventId,
                                   @RequestParam(name = "place") Integer place,
                                   @RequestParam(name = "category") String category) {
        ModelAndView mav = new ModelAndView("tickets");
        mav.addObject("tickets",
                List.of(bookingFacade.bookTicket(userId, eventId, place, Ticket.Category.valueOf(category))));
        return mav;
    }

    @GetMapping("/byUser/{userId}")
    public ModelAndView getBookedTicketsByUser(@PathVariable("userId") Long userId,
                                      @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNum,
                                      @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize) {
        ModelAndView mav = new ModelAndView("tickets");
        User user = new UserImpl();
        user.setId(userId);
        mav.addObject("tickets", bookingFacade.getBookedTickets(user, pageSize, pageNum));
        return mav;
    }

    @GetMapping("/byEvent/{eventId}")
    public ModelAndView getBookedTicketsByEvent(@PathVariable("eventId") Long eventId,
                                         @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNum,
                                         @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize) {
        ModelAndView mav = new ModelAndView("tickets");
        Event event = new EventImpl();
        event.setId(eventId);
        mav.addObject("tickets", bookingFacade.getBookedTickets(event, pageSize, pageNum));
        return mav;
    }

    @DeleteMapping("/{id}")
    public ModelAndView cancelTicket(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("tickets");
        mav.addObject("isTicketCanceled", bookingFacade.cancelTicket(id));
        return mav;
    }

    @PostMapping("/preload")
    public ModelAndView handleFileUpload(@RequestParam("file") MultipartFile file) throws IOException {
        ModelAndView mav = new ModelAndView("tickets");
        List<Ticket> preloadedTickets = bookingFacade.preloadTickets(file.getInputStream());
        mav.addObject("tickets", preloadedTickets);
        return mav;
    }
}
