package com.epam.events.tickets.booking.exception;

import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.AbstractHandlerExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomHandlerExceptionResolver extends AbstractHandlerExceptionResolver {

    @Override
    protected ModelAndView doResolveException(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler,
            Exception ex) {
        try {
            if (ex instanceof EventNotFoundException || ex instanceof UserNotFoundException) {
                return handleNotFoundException(ex);
            }
            if (ex instanceof IllegalStateException) {
                return handleIllegalStateException(ex);
            }
            if (ex instanceof IOException) {
                return handleIoException(ex);
            }
        } catch (Exception handlerException) {
            logger.warn("Handling of [" + ex.getClass().getName() + "]resulted in Exception", handlerException);
        }
        return null;
    }

    private ModelAndView handleNotFoundException(Exception ex) {
        ModelAndView mav = new ModelAndView("error");
        String stringBuilder = "Entity wasn't found. " +
                "The following exception was thrown: " +
                ex.getClass().getName();
        mav.addObject("errorMsg", stringBuilder);
        return mav;
    }

    private ModelAndView handleIllegalStateException(Exception ex) {
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMsg", "Ticket for this place is already booked!");
        return mav;
    }

    private ModelAndView handleIoException(Exception ex) {
        ModelAndView mav = new ModelAndView("error");
        mav.addObject("errorMsg", "Error while reading data from preload file");
        return mav;
    }
}
