package com.epam.events.tickets.booking.model.impl;

import com.epam.events.tickets.booking.model.User;
import lombok.Data;

@Data
public class UserImpl implements User {
    private long id;
    private String name;
    private String email;
}
