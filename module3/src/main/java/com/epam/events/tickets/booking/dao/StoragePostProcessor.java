package com.epam.events.tickets.booking.dao;

import com.epam.events.tickets.booking.constants.Deliminator;
import com.epam.events.tickets.booking.constants.Namespace;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.parser.EventParser;
import com.epam.events.tickets.booking.parser.TicketParser;
import com.epam.events.tickets.booking.parser.UserParser;
import lombok.Setter;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class StoragePostProcessor implements BeanPostProcessor {

    @Setter
    private String fileName;

    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        if (bean instanceof Storage) {
            processStorageBean((Storage) bean);
        }
        return bean;
    }

    private void processStorageBean(Storage storage) {
        try (Scanner scanner = new Scanner(new File(fileName))){
            while (scanner.hasNextLine()) {
                String row = scanner.nextLine();
                populateStorageWithRowData(storage, row);
            }
        } catch (FileNotFoundException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void populateStorageWithRowData(Storage storage, String row) throws ParseException {
        String namespace = row.split(Deliminator.COLON)[0];
        List<String> objectData = getObjectData(row);
        long freeId = storage.getNextFreeIdForNamespace(namespace);
        String key = namespace + Deliminator.COLON + freeId;
        switch (namespace) {
            case Namespace.EVENT -> {
                Event event = EventParser.parse(freeId, objectData);
                storage.put(key, event);
            }
            case Namespace.USER -> {
                User user = UserParser.parse(freeId, objectData);
                storage.put(key, user);
            }
            case Namespace.TICKET -> {
                Ticket ticket = TicketParser.parse(freeId, objectData);
                storage.put(key, ticket);
            }
        }
    }

    private List<String> getObjectData(String data) {
        return Arrays.stream(data
                .split(Deliminator.COLON)[1]
                .trim()
                .split(Deliminator.COMMA))
                .map(String::trim)
                .collect(Collectors.toList());
    }
}
