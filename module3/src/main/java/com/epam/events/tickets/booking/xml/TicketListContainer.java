package com.epam.events.tickets.booking.xml;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Root(name = "tickets")
public class TicketListContainer {
    @ElementList(inline=true)
    private List<Ticket> list = new ArrayList<>();
}

