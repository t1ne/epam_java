package com.epam.events.tickets.booking.dao;

import com.epam.events.tickets.booking.constants.Deliminator;
import com.epam.events.tickets.booking.constants.Namespace;
import com.epam.events.tickets.booking.model.User;
import lombok.Setter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class UserDao implements Dao<User> {

    @Setter
    private Storage storage;

    @Override
    public Optional<User> get(long id) {
        return Optional.ofNullable((User) storage.get(Namespace.USER + Deliminator.COLON + id));
    }

    @Override
    public List<User> getAll() {
        return storage
                .getAllByNamespace(Namespace.USER)
                .stream()
                .map(storageEntry -> (User) storageEntry)
                .collect(Collectors.toList());
    }

    @Override
    public User save(User user) {
        long newId = storage.getNextFreeIdForNamespace(Namespace.USER);
        user.setId(newId);
        storage.put(Namespace.USER + Deliminator.COLON + newId, user);
        return user;
    }

    @Override
    public User update(User user) {
        storage.put(Namespace.USER + Deliminator.COLON + user.getId(), user);
        return user;
    }

    @Override
    public boolean delete(long id) {
        return storage.remove(Namespace.USER + Deliminator.COLON + id);
    }
}
