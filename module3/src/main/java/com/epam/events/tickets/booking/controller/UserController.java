package com.epam.events.tickets.booking.controller;

import com.epam.events.tickets.booking.exception.UserNotFoundException;
import com.epam.events.tickets.booking.facade.BookingFacade;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/users")
public class UserController {

    private final BookingFacade bookingFacade;

    public UserController(BookingFacade bookingFacade) {
        this.bookingFacade = bookingFacade;
    }

    @GetMapping
    public ModelAndView getIndexPage() {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", Collections.emptyList());
        return mav;
    }

    @GetMapping("/{id}")
    public ModelAndView getUserById(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", List.of(bookingFacade.getUserById(id)));
        return mav;
    }

    @GetMapping("/byEmail/{email}")
    public ModelAndView getUserByEmail(@PathVariable("email") String email) {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", List.of(bookingFacade.getUserByEmail(email)));
        return mav;
    }

    @GetMapping("/byName/{name}")
    public ModelAndView getUserByName(@PathVariable("name") String name,
                                      @RequestParam(name = "page", required = false, defaultValue = "1") Integer pageNum,
                                      @RequestParam(name = "size", required = false, defaultValue = "10") Integer pageSize) {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("users", bookingFacade.getUsersByName(name, pageSize, pageNum));
        return mav;
    }

    @PostMapping
    public ModelAndView createUser(@RequestParam(name = "name") String name,
                                      @RequestParam(name = "email") String email) {
        ModelAndView mav = new ModelAndView("users");
        User user = new UserImpl();
        user.setName(name);
        user.setEmail(email);
        mav.addObject("users", List.of(bookingFacade.createUser(user)));
        return mav;
    }

    @PutMapping("/{id}")
    public ModelAndView updateUser(@PathVariable("id") Long id,
                                   @RequestParam(name = "name") String name,
                                   @RequestParam(name = "email") String email) {
        ModelAndView mav = new ModelAndView("users");
        User user = new UserImpl();
        user.setId(id);
        user.setName(name);
        user.setEmail(email);
        mav.addObject("users", List.of(bookingFacade.updateUser(user)));
        return mav;
    }

    @DeleteMapping("/{id}")
    public ModelAndView deleteUser(@PathVariable("id") Long id) {
        ModelAndView mav = new ModelAndView("users");
        mav.addObject("isUserDeleted", bookingFacade.deleteUser(id));
        return mav;
    }
}
