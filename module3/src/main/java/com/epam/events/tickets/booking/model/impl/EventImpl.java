package com.epam.events.tickets.booking.model.impl;

import com.epam.events.tickets.booking.model.Event;
import lombok.Data;

import java.util.Date;

@Data
public class EventImpl implements Event {
    private long id;
    private String title;
    private Date date;
}
