package com.epam.events.tickets.booking.service;

import com.epam.events.tickets.booking.dao.Dao;
import com.epam.events.tickets.booking.exception.EventNotFoundException;
import com.epam.events.tickets.booking.model.Event;
import lombok.Setter;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class EventService {

    @Setter
    private Dao<Event> eventDao;

    public Event getEventById(long eventId) throws EventNotFoundException {
        return eventDao.get(eventId).orElseThrow(EventNotFoundException::new);
    }

    public List<Event> getEventsByTitle(String title, int pageSize, int pageNum) {
        return eventDao.getAll().stream()
                .filter(event -> event.getTitle().contains(title))
                .skip((long) (pageNum - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Event> getEventsForDay(Date date, int pageSize, int pageNum) {
        return eventDao.getAll().stream()
                .filter(event -> event.getDate().equals(date))
                .skip((long) (pageNum - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public Event createEvent(Event event) {
        return eventDao.save(event);
    }

    public Event updateEvent(Event event) {
        return eventDao.update(event);
    }

    public boolean deleteEvent(long eventId) {
        return eventDao.delete(eventId);
    }
}
