package com.epam.events.tickets.booking.dao;

import com.epam.events.tickets.booking.constants.Deliminator;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
public class Storage {

    private final Map<String, Object> storage = new HashMap<>();

    public Object get(String key) {
        log.info("Getting object from storage by key: " + key);
        return storage.get(key);
    }

    public List<Object> getAllByNamespace(String namespace) {
        log.info("Getting list of objects in storage by namespace:" + namespace);
        return storage.entrySet()
                .stream()
                .filter(entry -> entry.getKey().contains(namespace))
                .map(Map.Entry::getValue)
                .collect(Collectors.toList());
    }

    public long getNextFreeIdForNamespace(String namespace) {
        long nextFreeId = storage.keySet()
                .stream()
                .filter(x -> x.contains(namespace))
                .map(key -> Integer.parseInt(key.split(Deliminator.COLON)[1]))
                .max(Integer::compareTo).orElse(0) + 1;
        log.info("Next free id for " + namespace + " is " + nextFreeId);
        return nextFreeId;
    }

    public void put(String key, Object object) {
        log.info("Put into storage for key:" + key);
        storage.put(key, object);
    }

    public boolean remove(String key) {
        log.info("Removing from storage by key: " + key);
        if (storage.containsKey(key)) {
            storage.remove(key);
            return true;
        } else {
            return false;
        }
    }
}
