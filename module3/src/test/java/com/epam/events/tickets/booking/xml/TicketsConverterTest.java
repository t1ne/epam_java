package com.epam.events.tickets.booking.xml;

import org.junit.jupiter.api.Test;
import com.epam.events.tickets.booking.model.Ticket;

import java.io.FileInputStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;


class TicketsConverterTest {

    @Test
    void parseTicketsFromPredefinedFileTest() throws Exception {
        FileInputStream inputStream = new FileInputStream("tickets.xml");
        List<Ticket> list = TicketsConverter.parseTickets(inputStream);
        assertEquals(2, list.size());
    }
}