package com.epam.events.tickets.booking.controller;

import com.epam.events.tickets.booking.facade.BookingFacade;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(UserController.class)
@ComponentScan("com.epam.events.tickets.booking.controller")
@ContextConfiguration(locations = "/ApplicationContext.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookingFacade facade;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(facade);
    }

    @Test
    void indexPageIsReturnedTest() throws Exception {
        mockMvc.perform(get("/users"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("users"));
    }

    @SneakyThrows
    @Test
    void userIsReturnedByIdTest() {
        User user = new UserImpl();
        user.setId(1);
        when(facade.getUserById(1L)).thenReturn(user);

        mockMvc.perform(get("/users/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("users"))
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attribute("users", List.of(user)));

        verify(facade).getUserById(1L);
    }

    @SneakyThrows
    @Test
    void userIsReturnedByEmailTest() {
        String email = "josh@email.com";
        User user = new UserImpl();
        user.setId(1);
        user.setEmail(email);
        when(facade.getUserByEmail(email))
                .thenReturn(user);

        mockMvc.perform(get("/users/byEmail/" + email))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("users"))
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attribute("users", List.of(user)));

        verify(facade).getUserByEmail(email);
    }

    @SneakyThrows
    @Test
    void usersAreReturnedByNameTest() {
        String name = "Josh";
        User user = new UserImpl();
        user.setName(name);
        when(facade.getUsersByName(name, 10, 1))
                .thenReturn(List.of(user));

        mockMvc.perform(get("/users/byName/" + name))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("users"))
                .andExpect(model().attributeExists("users"))
                .andExpect(model().attribute("users", List.of(user)));

        verify(facade).getUsersByName(name, 10, 1);
    }

    @SneakyThrows
    @Test
    void userIsCreatedSuccessfullyTest() {
        String name = "Josh";
        String email = "josh@email.com";
        User user = new UserImpl();
        user.setEmail(email);
        user.setName(name);
        when(facade.createUser(user))
                .then(AdditionalAnswers.returnsFirstArg());

        MvcResult mvcResult =
                mockMvc.perform(post("/users")
                    .param("name", name)
                    .param("email", email))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("users"))
                .andExpect(model().attributeExists("users"))
                .andReturn();
        User createdUser = ((List<User>) mvcResult.getModelAndView().getModel().get("users")).get(0);
        assertEquals(name, createdUser.getName());
        assertEquals(email, createdUser.getEmail());

        verify(facade).createUser(user);
    }
}
