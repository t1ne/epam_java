package com.epam.events.tickets.booking.controller;

import com.epam.events.tickets.booking.facade.BookingFacade;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.impl.EventImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.AdditionalAnswers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(EventController.class)
@ComponentScan("com.epam.events.tickets.booking.controller")
@ContextConfiguration(locations = "/ApplicationContext.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class EventControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookingFacade facade;

    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(facade);
    }

    @Test
    void indexPageIsReturnedTest() throws Exception {
        mockMvc.perform(get("/events"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("events"));
    }

    @SneakyThrows
    @Test
    void eventIsReturnedByIdTest(){
        Event event = new EventImpl();
        event.setId(1);
        event.setTitle("Title");
        when(facade.getEventById(1L)).thenReturn(event);

        mockMvc.perform(get("/events/1"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("events"))
                .andExpect(model().attributeExists("events"))
                .andExpect(model().attribute("events", List.of(event)));

        verify(facade).getEventById(1L);
    }

    @SneakyThrows
    @Test
    void eventIsReturnedByTitleTest() {
        Event event = new EventImpl();
        event.setId(1);
        event.setTitle("Title");
        when(facade.getEventsByTitle("Title", 10, 1))
                .thenReturn(List.of(event));

        mockMvc.perform(get("/events/byTitle/Title"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("events"))
                .andExpect(model().attributeExists("events"))
                .andExpect(model().attribute("events", List.of(event)));

        verify(facade).getEventsByTitle("Title", 10, 1);
    }

    @SneakyThrows
    @Test
    void eventsAreReturnedForDayTest() {
        Date currentDate = trimHoursFromDate(new Date());
        Event event = new EventImpl();
        event.setId(1);
        event.setDate(currentDate);
        when(facade.getEventsForDay(currentDate, 10, 1))
                .thenReturn(List.of(event));

        mockMvc.perform(get("/events/byDay/" + df.format(currentDate)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("events"))
                .andExpect(model().attributeExists("events"))
                .andExpect(model().attribute("events", List.of(event)));

        verify(facade).getEventsForDay(currentDate, 10, 1);
    }

    @SneakyThrows
    @Test
    void eventIsCreatedSuccessfullyTest() {
        Date currentDate = trimHoursFromDate(new Date());
        Event event = new EventImpl();
        event.setTitle("Title");
        event.setDate(currentDate);
        when(facade.createEvent(event))
                .then(AdditionalAnswers.returnsFirstArg());

        MvcResult mvcResult =
                mockMvc.perform(post("/events")
                    .param("title", "Title")
                    .param("date", df.format(currentDate)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("events"))
                .andExpect(model().attributeExists("events"))
                .andReturn();
        Event createdEvent = ((List<Event>) mvcResult.getModelAndView().getModel().get("events")).get(0);
        assertEquals("Title", createdEvent.getTitle());
        assertEquals(currentDate, event.getDate());

        verify(facade).createEvent(event);
    }

    private Date trimHoursFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 3);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
