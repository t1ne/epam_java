package com.epam.events.tickets.booking.dao;

import com.epam.events.tickets.booking.constants.Namespace;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.impl.EventImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EventDaoTest {

    @InjectMocks
    Dao<Event> eventDao = new EventDao();

    @Mock
    Storage storageMock;

    @Test
    void eventIsRetrievedByIdCorrectlyTest() {
        Event newEvent = new EventImpl();
        newEvent.setId(1);
        newEvent.setTitle("Event title");
        when(storageMock.get("event:1")).thenReturn(newEvent);

        Optional<Event> returnedEvent = eventDao.get(1);
        assertTrue(returnedEvent.isPresent());
        assertEquals(newEvent, returnedEvent.get());
    }

    @Test
    void retrieveAllEventsFromStorageTest() {
        Event newEvent = new EventImpl();
        newEvent.setId(1);
        newEvent.setTitle("Event title");
        when(storageMock.getAllByNamespace(Namespace.EVENT)).thenReturn(List.of(newEvent));

        List<Event> returnedEvents = eventDao.getAll();
        assertEquals(1, returnedEvents.size());
        assertEquals(1, returnedEvents.stream().findFirst().get().getId());
    }

    @Test
    void savedEventIdIsReturnedTest() {
        Event newEvent = new EventImpl();
        newEvent.setTitle("Event title");
        when(storageMock.getNextFreeIdForNamespace(Namespace.EVENT)).thenReturn(1L);

        Event savedEvent = eventDao.save(newEvent);
        assertEquals(1, savedEvent.getId());
        assertEquals("Event title", savedEvent.getTitle());
    }
}