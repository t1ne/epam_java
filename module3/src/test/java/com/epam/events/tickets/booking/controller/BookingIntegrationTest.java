package com.epam.events.tickets.booking.controller;

import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import lombok.SneakyThrows;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.ModelAndView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(controllers = {EventController.class, UserController.class, TicketController.class})
@ComponentScan("com.epam.events.tickets.booking.controller")
@ContextConfiguration(locations = "/ApplicationContext.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class BookingIntegrationTest {
    @Autowired
    private MockMvc mockMvc;

    private final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    @SneakyThrows
    @Test
    public void givenUserAndEventExist_WhenNewTicketIsBooked_AndThenCanceled_NoTicketsAreReturnedTest() {
        ModelAndView userMav =
                mockMvc.perform(post("/users")
                .param("name", "Josh")
                .param("email", "josh@email.com"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("users"))
                .andExpect(model().attributeExists("users"))
                .andReturn().getModelAndView();
        Object usersResponse = userMav.getModel().get("users");
        User createdUser = ((List<User>) usersResponse).get(0);
        long userId = createdUser.getId();

        Date currentDate = trimHoursFromDate(new Date());
        ModelAndView eventMav =
                mockMvc.perform(post("/events")
                .param("title", "Title")
                .param("date", df.format(currentDate)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("events"))
                .andExpect(model().attributeExists("events"))
                .andReturn().getModelAndView();
        Object eventsResponse = eventMav.getModel().get("events");
        Event createdEvent = ((List<Event>) eventsResponse).get(0);
        long eventId = createdUser.getId();

        ModelAndView ticketsMav =
                mockMvc.perform(post("/tickets")
                .param("userId", String.valueOf(userId))
                .param("eventId", String.valueOf(eventId))
                .param("place", String.valueOf(1))
                .param("category", "BAR"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("tickets"))
                .andExpect(model().attributeExists("tickets"))
                .andReturn().getModelAndView();
        Object ticketsResponse = ticketsMav.getModel().get("tickets");
        Ticket bookedTicket = ((List<Ticket>) ticketsResponse).get(0);
        long ticketId = bookedTicket.getId();
        assertNotNull(bookedTicket);
        assertEquals(userId, bookedTicket.getUserId());
        assertEquals(eventId, bookedTicket.getEventId());

        mockMvc.perform(delete("/tickets/" + ticketId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("tickets"))
                .andExpect(model().attributeExists("isTicketCanceled"))
                .andExpect(model().attribute("isTicketCanceled", true));

        mockMvc.perform(get("/tickets/byUser/" + userId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("tickets"))
                .andExpect(model().attributeExists("tickets"))
                .andExpect(model().attribute("tickets", Collections.emptyList()));
    }

    private Date trimHoursFromDate(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 3);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
}
