package com.epam.events.tickets.booking.service;

import com.epam.events.tickets.booking.dao.Dao;
import com.epam.events.tickets.booking.exception.UserNotFoundException;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @InjectMocks
    UserService userService;

    @Mock
    Dao<User> userDaoMock;

    @Test
    void getUserByIdTest() {
        User newUser = new UserImpl();
        when(userDaoMock.get(1L)).thenReturn(Optional.of(newUser));

        AtomicReference<User> returnedUser = new AtomicReference<>();
        assertDoesNotThrow(() -> {
            returnedUser.set(userService.getUserById(1L));
        });
        assertEquals(newUser, returnedUser.get());
    }

    @Test
    void getNotExistingUserByIdTest() {
        when(userDaoMock.get(1L)).thenReturn(Optional.empty());

        assertThrows(UserNotFoundException.class, () -> {
            userService.getUserById(1L);
        });
    }

    @Test
    void getUserByEmailTest() {
        User user = new UserImpl();
        user.setEmail("john@email.com");
        when(userDaoMock.getAll()).thenReturn(List.of(user));

        AtomicReference<User> returnedUser = new AtomicReference<>();
        assertDoesNotThrow(() -> {
            returnedUser.set(userService.getUserByEmail("john@email.com"));
        });
        assertEquals(user, returnedUser.get());
    }

    @Test
    void getNotExistingUserByEmailTest() {
        User user = new UserImpl();
        user.setEmail("NOTjohn@NOTemail.com");
        when(userDaoMock.getAll()).thenReturn(List.of(user));

        assertThrows(UserNotFoundException.class, () -> {
            userService.getUserByEmail("wrongOne@email.com");
        });
    }

    @Test
    void getUsersByNameTest() {
        User user1 = new UserImpl();
        user1.setName("John Favro");
        User user2 = new UserImpl();
        user2.setName("John Wick");
        User user3 = new UserImpl();
        user3.setName("Anton");
        when(userDaoMock.getAll()).thenReturn(List.of(user1, user2, user3));

        List<User> returnedUsers = userService.getUsersByName("John", 3, 1);
        assertEquals(2, returnedUsers.size());
        assertTrue(returnedUsers.stream().anyMatch(user -> user.getName().equals("John Wick")));
    }

    @Test
    void getUsersByNamePaginationTest() {
        User user1 = new UserImpl();
        user1.setName("John Favro");
        User user2 = new UserImpl();
        user2.setName("John Wick");
        User user3 = new UserImpl();
        user3.setName("Another John");
        when(userDaoMock.getAll()).thenReturn(List.of(user1, user2, user3));

        List<User> firstPage = userService.getUsersByName("John", 2, 1);
        assertEquals(2, firstPage.size());
        assertTrue(firstPage.stream().anyMatch(user -> user.getName().equals("John Wick")));

        List<User> secondPage = userService.getUsersByName("John", 2, 2);
        assertEquals(1, secondPage.size());
        assertTrue(secondPage.stream().anyMatch(user -> user.getName().equals("Another John")));
    }
}