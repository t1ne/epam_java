package com.epam.events.tickets.booking.controller;

import com.epam.events.tickets.booking.facade.BookingFacade;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.EventImpl;
import com.epam.events.tickets.booking.model.impl.TicketImpl;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@WebMvcTest(TicketController.class)
@ComponentScan("com.epam.events.tickets.booking.controller")
@ContextConfiguration(locations = "/ApplicationContext.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class TicketControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookingFacade facade;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(facade);
    }

    @Test
    void indexPageIsReturnedTest() throws Exception {
        mockMvc.perform(get("/tickets"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("tickets"));
    }

    @SneakyThrows
    @Test
    void ticketIsBookedSuccessfullyTest() {
        long userId = 10L;
        long eventId = 10L;
        int place = 1;
        Ticket.Category category = Ticket.Category.BAR;
        Ticket ticket = new TicketImpl();
        ticket.setUserId(userId);
        ticket.setEventId(eventId);
        ticket.setPlace(place);
        ticket.setCategory(category);
        when(facade.bookTicket(userId, eventId, place, category))
                .thenReturn(ticket);

        MvcResult mvcResult =
                mockMvc.perform(post("/tickets")
                        .param("userId", String.valueOf(userId))
                        .param("eventId", String.valueOf(eventId))
                        .param("place", String.valueOf(place))
                        .param("category", category.toString()))
                        .andDo(print())
                        .andExpect(status().isOk())
                        .andExpect(view().name("tickets"))
                        .andExpect(model().attributeExists("tickets"))
                        .andReturn();
        Ticket bookedTicket = ((List<Ticket>) mvcResult.getModelAndView().getModel().get("tickets")).get(0);
        assertEquals(userId, bookedTicket.getUserId());
        assertEquals(eventId, bookedTicket.getEventId());
        assertEquals(place, bookedTicket.getPlace());
        assertEquals(category, bookedTicket.getCategory());

        verify(facade).bookTicket(userId, eventId, place, category);
    }

    @SneakyThrows
    @Test
    void bookedTicketsAreReturnedForUserTest() {
        User user = new UserImpl();
        long userId = 1L;
        user.setId(userId);
        Ticket ticket = new TicketImpl();
        ticket.setUserId(userId);
        when(facade.getBookedTickets(user, 10, 1))
                .thenReturn(List.of(ticket));

        mockMvc.perform(get("/tickets/byUser/" + userId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("tickets"))
                .andExpect(model().attributeExists("tickets"))
                .andExpect(model().attribute("tickets", List.of(ticket)));

        verify(facade).getBookedTickets(user, 10, 1);
    }

    @SneakyThrows
    @Test
    void bookedTicketsAreReturnedForEventTest() {
        Event event = new EventImpl();
        long eventId = 1L;
        event.setId(eventId);
        Ticket ticket = new TicketImpl();
        ticket.setEventId(eventId);
        when(facade.getBookedTickets(event, 10, 1))
                .thenReturn(List.of(ticket));

        mockMvc.perform(get("/tickets/byEvent/" + eventId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("tickets"))
                .andExpect(model().attributeExists("tickets"))
                .andExpect(model().attribute("tickets", List.of(ticket)));

        verify(facade).getBookedTickets(event, 10, 1);
    }

    @SneakyThrows
    @Test
    void ticketIsCanceledTest() {
        long ticketId = 1L;
        when(facade.cancelTicket(ticketId))
                .thenReturn(true);

        mockMvc.perform(delete("/tickets/" + ticketId))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(view().name("tickets"))
                .andExpect(model().attributeExists("isTicketCanceled"))
                .andExpect(model().attribute("isTicketCanceled", true));

        verify(facade).cancelTicket(ticketId);
    }
}
