package com.epam.events.tickets.booking.dao;

import com.epam.events.tickets.booking.constants.Namespace;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.impl.TicketImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TicketDaoTest {

    @InjectMocks
    Dao<Ticket> ticketDao = new TicketDao();

    @Mock
    Storage storageMock;

    @Test
    void ticketIsRetrievedByIdCorrectlyTest() {
        Ticket newTicket = new TicketImpl();
        newTicket.setId(1);
        newTicket.setCategory(Ticket.Category.STANDARD);
        when(storageMock.get("ticket:1")).thenReturn(newTicket);

        Optional<Ticket> returnedTicket = ticketDao.get(1);
        assertTrue(returnedTicket.isPresent());
        assertEquals(newTicket, returnedTicket.get());
    }

    @Test
    void retrieveAllTicketsFromStorageTest() {
        Ticket newTicket = new TicketImpl();
        newTicket.setId(1);
        newTicket.setCategory(Ticket.Category.STANDARD);
        when(storageMock.getAllByNamespace(Namespace.TICKET)).thenReturn(List.of(newTicket));

        List<Ticket> returnedTickets = ticketDao.getAll();
        assertEquals(1, returnedTickets.size());
        assertEquals(1, returnedTickets.stream().findFirst().get().getId());
    }

    @Test
    void savedTicketIdIsReturnedTest() {
        Ticket newTicket = new TicketImpl();
        newTicket.setId(1);
        newTicket.setCategory(Ticket.Category.STANDARD);
        when(storageMock.getNextFreeIdForNamespace(Namespace.TICKET)).thenReturn(1L);

        Ticket savedTicket = ticketDao.save(newTicket);
        assertEquals(1, savedTicket.getId());
        assertEquals(Ticket.Category.STANDARD, savedTicket.getCategory());
    }
}