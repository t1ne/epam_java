package com.epam.events.tickets.booking.facade;

import com.epam.events.tickets.booking.exception.UserNotFoundException;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.EventImpl;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = {"/ApplicationContext.xml"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookingFacadeIntegrationTest {
    @Autowired
    private BookingFacade bookingFacade;

    private final int PAGE_SIZE = 100;

    @Test
    public void givenUserAndEventExist_WhenNewTicketIsBooked_AndThenCanceled_NoTicketsAreReturnedTest() {
        User user = new UserImpl();
        user.setName("John");
        User createdUser = bookingFacade.createUser(user);

        Event event = new EventImpl();
        event.setTitle("Symphonic orchestra concert");
        Event createdEvent = bookingFacade.createEvent(event);

        Ticket bookedTicket = bookingFacade.bookTicket(
                createdUser.getId(), createdEvent.getId(), 1, Ticket.Category.STANDARD);
        List<Ticket> bookedTicketsForUser = bookingFacade.getBookedTickets(user, PAGE_SIZE, 1);
        assertFalse(bookedTicketsForUser.isEmpty());
        assertTrue(bookedTicketsForUser.stream().anyMatch(ticket -> ticket.getEventId() == createdEvent.getId()));

        bookingFacade.cancelTicket(bookedTicket.getId());
        bookedTicketsForUser = bookingFacade.getBookedTickets(user, PAGE_SIZE, 1);
        assertTrue(bookedTicketsForUser.isEmpty());
    }

    @Test
    public void givenUserAndEventExist_WhenExisitingTicketIsBookedThenErrorIsThrownTest() {
        User user = new UserImpl();
        user.setName("John");
        User createdUser = bookingFacade.createUser(user);
        User anotherUser = bookingFacade.createUser(new UserImpl());

        Event event = new EventImpl();
        event.setTitle("Symphonic orchestra concert");
        Event createdEvent = bookingFacade.createEvent(event);

        bookingFacade.bookTicket(
                createdUser.getId(),
                createdEvent.getId(),
                1,
                Ticket.Category.STANDARD);
        List<Ticket> bookedTicketsForUser = bookingFacade.getBookedTickets(user, PAGE_SIZE, 1);
        assertFalse(bookedTicketsForUser.isEmpty());
        assertTrue(bookedTicketsForUser.stream().anyMatch(ticket -> ticket.getEventId() == createdEvent.getId()));

        assertThrows(IllegalStateException.class,
                () -> bookingFacade.bookTicket(
                        anotherUser.getId(),
                        createdEvent.getId(),
                        1,
                        Ticket.Category.STANDARD));;
    }

    @Test
    public void whenFewUsersBookTicketsForEvent_ThenAllUsersWhoAreAttendingEventAreReturnedTest() {
        User user1 = new UserImpl();
        user1.setName("John");
        User createdUser1 = bookingFacade.createUser(user1);
        User user2 = new UserImpl();
        user2.setName("John");
        User createdUser2 = bookingFacade.createUser(user2);
        User user3 = new UserImpl();
        user3.setName("John");
        User createdUser3 = bookingFacade.createUser(user3);

        Event event = new EventImpl();
        event.setTitle("Symphonic orchestra concert");
        Event createdEvent = bookingFacade.createEvent(event);

        bookingFacade.bookTicket(
                createdUser1.getId(), createdEvent.getId(), 1, Ticket.Category.STANDARD);
        bookingFacade.bookTicket(
                createdUser2.getId(), createdEvent.getId(), 2, Ticket.Category.STANDARD);
        bookingFacade.bookTicket(
                createdUser3.getId(), createdEvent.getId(), 3, Ticket.Category.STANDARD);
        List<Ticket> bookedTicketsForEvent = bookingFacade.getBookedTickets(createdEvent, PAGE_SIZE, 1);
        assertFalse(bookedTicketsForEvent.isEmpty());
        assertTrue(bookedTicketsForEvent.stream().allMatch(ticket -> ticket.getEventId() == createdEvent.getId()));

        List<User> eventAttendees = bookedTicketsForEvent.stream().map(Ticket::getUserId).map(userId -> {
            try {
                return bookingFacade.getUserById(userId);
            } catch (UserNotFoundException e) {
                e.printStackTrace();
                return null;
            }
        }).filter(Objects::nonNull).collect(Collectors.toList());
        assertFalse(eventAttendees.isEmpty());
        assertEquals(3, eventAttendees.size());
        assertTrue(eventAttendees.stream().allMatch(attendee -> attendee.getName().equals("John")));
    }
}
