package com.epam.events.tickets.booking.dao;

import com.epam.events.tickets.booking.constants.Namespace;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserDaoTest {

    @Mock
    Storage storageMock;

    @Test
    void userIsRetrievedByIdCorrectlyTest() {
        UserDao userDao = new UserDao();
        userDao.setStorage(storageMock);

        User newUser = new UserImpl();
        newUser.setId(1);
        newUser.setName("John");
        when(storageMock.get("user:1")).thenReturn(newUser);

        Optional<User> returnedUser = userDao.get(1);
        assertTrue(returnedUser.isPresent());
        assertEquals(newUser, returnedUser.get());
    }

    @Test
    void retrieveAllUsersFromStorageTest() {
        UserDao userDao = new UserDao();
        userDao.setStorage(storageMock);

        User newUser = new UserImpl();
        newUser.setId(1);
        newUser.setName("John");
        when(storageMock.getAllByNamespace(Namespace.USER)).thenReturn(List.of(newUser));

        List<User> returnedUsers = userDao.getAll();
        assertEquals(1, returnedUsers.size());
        assertEquals(1, returnedUsers.stream().findFirst().get().getId());
    }

    @Test
    void savedUserIdIsReturnedTest() {
        UserDao userDao = new UserDao();
        userDao.setStorage(storageMock);

        User newUser = new UserImpl();
        newUser.setName("John");
        when(storageMock.getNextFreeIdForNamespace(Namespace.USER)).thenReturn(1L);

        User savedUser = userDao.save(newUser);
        assertEquals(1, savedUser.getId());
        assertEquals("John", savedUser.getName());
    }
}