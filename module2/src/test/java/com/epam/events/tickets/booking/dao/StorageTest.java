package com.epam.events.tickets.booking.dao;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StorageTest {

    @Test
    void nullIsReturnedForNotExistingItemTest() {
        Storage storage = new Storage();
        Object returnedValue = storage.get("test");
        assertNull(returnedValue);
    }

    @Test
    void existingObjectIsCorrectlyReturnedTest() {
        Storage storage = new Storage();
        Object object = new Object();
        storage.put("1", object);
        Object returnedObject = storage.get("1");
        assertEquals(object, returnedObject);
    }

    @Test
    void idIsGeneratedForNotPresentInStorageNamespaceTest() {
        Storage storage = new Storage();
        String namespace = "example";
        long generatedId = storage.getNextFreeIdForNamespace(namespace);
        assertEquals(1, generatedId);
    }

    @Test
    void idIsGeneratedForPresentInStorageNamespaceTest() {
        Storage storage = new Storage();
        String namespace = "example";
        storage.put(namespace + ":1", new Object());
        long generatedId = storage.getNextFreeIdForNamespace(namespace);
        assertEquals(2, generatedId);
    }

    @Test
    void allItemsAreReturnedForGivenNamespaceTest() {
        Storage storage = new Storage();
        String namespace = "example";
        storage.put(namespace + ":1", new Object());
        storage.put(namespace + ":2", new Object());
        storage.put(namespace + ":454", new Object());
        storage.put("anotherNamespace:2", new Object());
        List<Object> returnedList = storage.getAllByNamespace(namespace);
        assertEquals(3, returnedList.size());
    }

    @Test
    void objectIsRemovedTest() {
        Storage storage = new Storage();
        storage.put("example:1", new Object());
        boolean isRemoved = storage.remove("example:1");
        assertTrue(isRemoved);
        assertNull(storage.get("example:1"));
    }
}