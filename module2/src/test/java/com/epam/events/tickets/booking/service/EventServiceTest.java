package com.epam.events.tickets.booking.service;

import com.epam.events.tickets.booking.dao.Dao;
import com.epam.events.tickets.booking.exception.EventNotFoundException;
import com.epam.events.tickets.booking.exception.UserNotFoundException;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.EventImpl;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class EventServiceTest {

    @Mock
    Dao<Event> eventDaoMock;

    @Test
    void getEventByIdTest() {
        EventService eventService = new EventService();
        eventService.setEventDao(eventDaoMock);

        Event newEvent = new EventImpl();
        when(eventDaoMock.get(1L)).thenReturn(Optional.of(newEvent));

        AtomicReference<Event> returnedEvent = new AtomicReference<>();
        assertDoesNotThrow(() -> {
            returnedEvent.set(eventService.getEventById(1L));
        });
        assertEquals(newEvent, returnedEvent.get());
    }

    @Test
    void getNotExistingEventByIdTest() {
        EventService eventService = new EventService();
        eventService.setEventDao(eventDaoMock);

        when(eventDaoMock.get(1L)).thenReturn(Optional.empty());

        assertThrows(EventNotFoundException.class, () -> {
            eventService.getEventById(1L);
        });
    }

    @Test
    void getEventsByTitleTest() {
        EventService eventService = new EventService();
        eventService.setEventDao(eventDaoMock);

        Event event1 = new EventImpl();
        event1.setTitle("example");
        Event event2 = new EventImpl();
        event2.setTitle("example 2");
        Event event3 = new EventImpl();
        event3.setTitle("another example");
        when(eventDaoMock.getAll()).thenReturn(List.of(event1, event2, event3));

        List<Event> returnedEvents = eventService.getEventsByTitle("example", 3, 1);
        assertEquals(3, returnedEvents.size());
        assertTrue(returnedEvents.stream().anyMatch(event -> event.getTitle().equals("another example")));
    }

    @Test
    void getEventsByTitlePaginationTest() {
        EventService eventService = new EventService();
        eventService.setEventDao(eventDaoMock);

        Event event1 = new EventImpl();
        event1.setTitle("example");
        Event event2 = new EventImpl();
        event2.setTitle("example 2");
        Event event3 = new EventImpl();
        event3.setTitle("another example");
        when(eventDaoMock.getAll()).thenReturn(List.of(event1, event2, event3));

        List<Event> firstPage = eventService.getEventsByTitle("example", 2, 1);
        assertEquals(2, firstPage.size());
        assertTrue(firstPage.stream().anyMatch(event -> event.getTitle().equals("example 2")));

        List<Event> secondPage = eventService.getEventsByTitle("example", 2, 2);
        assertEquals(1, secondPage.size());
        assertTrue(secondPage.stream().anyMatch(event -> event.getTitle().equals("another example")));
    }

    @Test
    void getEventsByDateTest() {
        EventService eventService = new EventService();
        eventService.setEventDao(eventDaoMock);

        Date date = Calendar.getInstance().getTime();
        Event event1 = new EventImpl();
        event1.setDate(date);
        Event event2 = new EventImpl();
        event2.setDate(date);
        Event event3 = new EventImpl();
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.MONTH, -1);
        event3.setDate(instance.getTime());
        when(eventDaoMock.getAll()).thenReturn(List.of(event1, event2, event3));

        List<Event> returnedEvents = eventService.getEventsForDay(date, 3, 1);
        assertEquals(2, returnedEvents.size());
        assertTrue(returnedEvents.stream().allMatch(event -> event.getDate().equals(date)));
    }

    @Test
    void getEventsByDatePaginationTest() {
        EventService eventService = new EventService();
        eventService.setEventDao(eventDaoMock);

        Date date = Calendar.getInstance().getTime();
        Event event1 = new EventImpl();
        event1.setDate(date);
        Event event2 = new EventImpl();
        event2.setDate(date);
        Event event3 = new EventImpl();
        event3.setDate(date);
        when(eventDaoMock.getAll()).thenReturn(List.of(event1, event2, event3));

        List<Event> firstPage = eventService.getEventsForDay(date, 2, 1);
        assertEquals(2, firstPage.size());
        assertTrue(firstPage.stream().allMatch(event -> event.getDate().equals(date)));

        List<Event> secondPage = eventService.getEventsForDay(date, 2, 2);
        assertEquals(1, secondPage.size());
        assertTrue(secondPage.stream().anyMatch(event -> event.getDate().equals(date)));
    }
}