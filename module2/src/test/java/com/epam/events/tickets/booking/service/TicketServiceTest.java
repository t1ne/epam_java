package com.epam.events.tickets.booking.service;

import com.epam.events.tickets.booking.dao.Dao;
import com.epam.events.tickets.booking.exception.EventNotFoundException;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.EventImpl;
import com.epam.events.tickets.booking.model.impl.TicketImpl;
import com.epam.events.tickets.booking.model.impl.UserImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TicketServiceTest {

    @Mock
    Dao<Ticket> ticketDaoMock;

    @Test
    void bookTicketTest() {
        TicketService ticketService = new TicketService();
        ticketService.setTicketDao(ticketDaoMock);

        Ticket ticket = new TicketImpl();
        ticket.setUserId(1L);
        ticket.setEventId(1L);
        ticket.setPlace(1);
        ticket.setCategory(Ticket.Category.STANDARD);
        when(ticketDaoMock.getAll()).thenReturn(Collections.emptyList());
        when(ticketDaoMock.save(ticket)).thenReturn(ticket);

        Ticket bookedTicket = ticketService.bookTicket(1L, 1L, 1, Ticket.Category.STANDARD);

        assertEquals(1L, bookedTicket.getUserId());
        assertEquals(1L, bookedTicket.getEventId());
        assertEquals(1, bookedTicket.getPlace());
        assertEquals(Ticket.Category.STANDARD, bookedTicket.getCategory());
    }

    @Test
    void bookingTicketForAlreadyBookedEventAndPlaceThrowsExceptionTest() {
        TicketService ticketService = new TicketService();
        ticketService.setTicketDao(ticketDaoMock);

        Ticket bookedTicket = new TicketImpl();
        bookedTicket.setUserId(1L);
        bookedTicket.setEventId(1L);
        bookedTicket.setPlace(1);
        when(ticketDaoMock.getAll()).thenReturn(List.of(bookedTicket));

        assertThrows(IllegalStateException.class, () -> {
            ticketService.bookTicket(2L, 1L, 1, Ticket.Category.STANDARD);
        });
    }

    @Test
    void getBookedTicketsForUserPagingTest() {
        TicketService ticketService = new TicketService();
        ticketService.setTicketDao(ticketDaoMock);

        User user = new UserImpl();
        user.setId(1L);
        Ticket ticket1 = new TicketImpl();
        ticket1.setUserId(user.getId());
        Ticket ticket2 = new TicketImpl();
        ticket2.setUserId(user.getId());
        Ticket ticket3 = new TicketImpl();
        ticket3.setUserId(user.getId());
        when(ticketDaoMock.getAll()).thenReturn(List.of(ticket1, ticket2, ticket3));

        List<Ticket> firstPage = ticketService.getBookedTickets(user, 2, 1);
        assertEquals(2, firstPage.size());

        List<Ticket> secondPage = ticketService.getBookedTickets(user, 2, 2);
        assertEquals(1, secondPage.size());
    }

    @Test
    void getBookedTicketsForEventPagingTest() {
        TicketService ticketService = new TicketService();
        ticketService.setTicketDao(ticketDaoMock);

        Event event = new EventImpl();
        event.setId(1L);
        Ticket ticket1 = new TicketImpl();
        ticket1.setEventId(event.getId());
        Ticket ticket2 = new TicketImpl();
        ticket2.setEventId(event.getId());
        Ticket ticket3 = new TicketImpl();
        ticket3.setEventId(event.getId());
        when(ticketDaoMock.getAll()).thenReturn(List.of(ticket1, ticket2, ticket3));

        List<Ticket> firstPage = ticketService.getBookedTickets(event, 2, 1);
        assertEquals(2, firstPage.size());

        List<Ticket> secondPage = ticketService.getBookedTickets(event, 2, 2);
        assertEquals(1, secondPage.size());
    }
}