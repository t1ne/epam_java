package com.epam.events.tickets.booking.facade.impl;

import com.epam.events.tickets.booking.exception.EventNotFoundException;
import com.epam.events.tickets.booking.facade.BookingFacade;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.exception.UserNotFoundException;
import com.epam.events.tickets.booking.service.EventService;
import com.epam.events.tickets.booking.service.TicketService;
import com.epam.events.tickets.booking.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class BookingFacadeImpl implements BookingFacade {
    private final UserService userService;
    private final EventService eventService;
    private final TicketService ticketService;

    @Override
    public Event getEventById(long eventId) throws EventNotFoundException {
        log.info("Getting event by id: " + eventId);
        return eventService.getEventById(eventId);
    }

    @Override
    public List<Event> getEventsByTitle(String title, int pageSize, int pageNum) {
        log.info("Getting events by title: " + title);
        log.info("Page parameter: pageNum = " + pageNum + "; pageSize = " + pageSize);
        return eventService.getEventsByTitle(title, pageSize, pageNum);
    }

    @Override
    public List<Event> getEventsForDay(Date day, int pageSize, int pageNum) {
        log.info("Getting events by date: " + day);
        log.info("Page parameter: pageNum = " + pageNum + "; pageSize = " + pageSize);
        return eventService.getEventsForDay(day, pageSize, pageNum);
    }

    @Override
    public Event createEvent(Event event) {
        log.info("Creating event: title=" + event.getTitle() + "; date=" + event.getDate());
        return eventService.createEvent(event);
    }

    @Override
    public Event updateEvent(Event event) {
        log.info("Updating event with id: " + event.getId());
        return eventService.updateEvent(event);
    }

    @Override
    public boolean deleteEvent(long eventId) {
        log.info("Deleting event for id: " + eventId);
        return eventService.deleteEvent(eventId);
    }

    @Override
    public User getUserById(long userId) throws UserNotFoundException {
        log.info("Getting user by id: " + userId);
        return userService.getUserById(userId);
    }

    @Override
    public User getUserByEmail(String email) throws UserNotFoundException {
        log.info("Getting users by email: " + email);
        return userService.getUserByEmail(email);
    }

    @Override
    public List<User> getUsersByName(String name, int pageSize, int pageNum) {
        log.info("Getting users by name: " + name);
        log.info("Page parameter: pageNum = " + pageNum + "; pageSize = " + pageSize);
        return userService.getUsersByName(name, pageSize, pageNum);
    }

    @Override
    public User createUser(User user) {
        log.info("Creating user: name=" + user.getName() + "; email=" + user.getEmail());
        return userService.createUser(user);
    }

    @Override
    public User updateUser(User user) {
        log.info("Update user with id: " + user.getId());
        return userService.updateUser(user);
    }

    @Override
    public boolean deleteUser(long userId) {
        log.info("Deleting user by id: " + userId);
        return userService.deleteUser(userId);
    }

    @Override
    public Ticket bookTicket(long userId, long eventId, int place, Ticket.Category category) {
        log.info("Booking ticket: userId=" + userId + "; eventId=" + eventId + "; place=" + place
                +  "; category=" + category.name());
        return ticketService.bookTicket(userId, eventId, place, category);
    }

    @Override
    public List<Ticket> getBookedTickets(User user, int pageSize, int pageNum) {
        log.info("Getting booked tickets by user: " + user.getName());
        log.info("Page parameter: pageNum = " + pageNum + "; pageSize = " + pageSize);
        return ticketService.getBookedTickets(user, pageSize, pageNum);
    }

    @Override
    public List<Ticket> getBookedTickets(Event event, int pageSize, int pageNum) {
        log.info("Getting event by title: " + event.getTitle());
        log.info("Page parameter: pageNum = " + pageNum + "; pageSize = " + pageSize);
        return ticketService.getBookedTickets(event, pageSize, pageNum);
    }

    @Override
    public boolean cancelTicket(long ticketId) {
        log.info("Cancelling ticket for id: " + ticketId);
        return ticketService.cancelTicket(ticketId);
    }
}
