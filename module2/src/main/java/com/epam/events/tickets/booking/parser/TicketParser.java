package com.epam.events.tickets.booking.parser;

import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.impl.TicketImpl;

import java.util.List;

public class TicketParser {
    private static final int EVENT_ID_INDEX = 0;
    private static final int USER_ID_INDEX = 1;
    private static final int CATEGORY_INDEX = 2;
    private static final int PLACE_INDEX = 3;

    public static Ticket parse(long id, List<String> objectData) {
        Ticket ticket = new TicketImpl();
        ticket.setId(id);
        ticket.setEventId(Integer.parseInt(objectData.get(EVENT_ID_INDEX)));
        ticket.setUserId(Integer.parseInt(objectData.get(USER_ID_INDEX)));
        ticket.setCategory(Ticket.Category.valueOf(objectData.get(CATEGORY_INDEX)));
        ticket.setPlace(Integer.parseInt(objectData.get(PLACE_INDEX)));
        return ticket;
    }
}
