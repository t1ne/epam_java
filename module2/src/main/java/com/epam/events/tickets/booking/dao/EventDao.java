package com.epam.events.tickets.booking.dao;

import com.epam.events.tickets.booking.constants.Deliminator;
import com.epam.events.tickets.booking.constants.Namespace;
import com.epam.events.tickets.booking.model.Event;
import lombok.Setter;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class EventDao implements Dao<Event> {
    @Setter
    private Storage storage;

    @Override
    public Optional<Event> get(long id) {
        return Optional.ofNullable((Event) storage.get(Namespace.EVENT + Deliminator.COLON + id));
    }

    @Override
    public List<Event> getAll() {
        return storage
                .getAllByNamespace(Namespace.EVENT)
                .stream()
                .map(storageEntry -> (Event) storageEntry)
                .collect(Collectors.toList());
    }

    @Override
    public Event save(Event event) {
        long newId = storage.getNextFreeIdForNamespace(Namespace.EVENT);
        event.setId(newId);
        storage.put(Namespace.EVENT + Deliminator.COLON  + newId, event);
        return event;
    }

    @Override
    public Event update(Event event) {
        storage.put(Namespace.EVENT + Deliminator.COLON + event.getId(), event);
        return event;
    }

    @Override
    public boolean delete(long id) {
        return storage.remove(Namespace.EVENT + Deliminator.COLON + id);
    }
}
