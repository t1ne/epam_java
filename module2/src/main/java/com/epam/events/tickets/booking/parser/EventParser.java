package com.epam.events.tickets.booking.parser;

import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.impl.EventImpl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

public class EventParser {
    private static final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

    private static final int TITLE_INDEX = 0;
    private static final int DATE_INDEX = 1;

    public static Event parse(long id, List<String> objectData) throws ParseException {
        Event event = new EventImpl();
        event.setId(id);
        event.setTitle(objectData.get(TITLE_INDEX));
        event.setDate(dateFormat.parse(objectData.get(DATE_INDEX)));
        return event;
    }
}
