package com.epam.events.tickets.booking.service;

import com.epam.events.tickets.booking.dao.Dao;
import com.epam.events.tickets.booking.model.Event;
import com.epam.events.tickets.booking.model.Ticket;
import com.epam.events.tickets.booking.model.User;
import com.epam.events.tickets.booking.model.impl.TicketImpl;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

public class TicketService {
    @Setter
    private Dao<Ticket> ticketDao;

    public Ticket bookTicket(long userId, long eventId, int place, Ticket.Category category) {
        if (ticketDao.getAll().stream()
                .anyMatch(ticket -> ticket.getEventId() == eventId && ticket.getPlace() == place)) {
            throw new IllegalStateException();
        } else {
            Ticket ticket = new TicketImpl();
            ticket.setUserId(userId);
            ticket.setEventId(eventId);
            ticket.setPlace(place);
            ticket.setCategory(category);
            return ticketDao.save(ticket);
        }
    }

    public List<Ticket> getBookedTickets(User user, int pageSize, int pageNum) {
        return ticketDao.getAll().stream()
                .filter(ticket -> ticket.getUserId() == user.getId())
                .skip((long) (pageNum - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Ticket> getBookedTickets(Event event, int pageSize, int pageNum) {
        return ticketDao.getAll().stream()
                .filter(ticket -> ticket.getEventId() == event.getId())
                .skip((long) (pageNum - 1) * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public boolean cancelTicket(long ticketId) {
        return ticketDao.delete(ticketId);
    }
}
