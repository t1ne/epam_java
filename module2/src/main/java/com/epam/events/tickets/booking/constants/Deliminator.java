package com.epam.events.tickets.booking.constants;

public class Deliminator {
    private Deliminator() {}

    public static final String COLON = ":";
    public static final String COMMA = ",";
}
