package com.epam.analyzer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.List;
import java.util.Optional;

@SpringBootApplication
@Slf4j
public class DiskAnalyzerApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DiskAnalyzerApplication.class, args);
	}

	@Override
	public void run(String... args) {
		if (args.length != 2) {
			log.error("Missing two mandatory launch parameters: \n" +
					"1 - Initial directory\n" +
					"2 - Function to be executed");
		}
		Path initialPath = Path.of(args[0]);
		if (Files.exists(initialPath)) {
			int functionNumber = Integer.parseInt(args[1]);
			if (functionNumber < 1 || functionNumber > 4) {
				log.error("Incorrect function number. Refer to README.md for more info.");
			} else {
				switch (functionNumber) {
					case 1 -> searchForTheFileWithMaxOccurrencesOfTheLetterSInName(initialPath);
					case 2 -> printTop5LargestFilesBySizeInBytes(initialPath);
					case 3 -> printAverageFileSizeInSpecifiedDirectoryAndItsSubdirectories(initialPath);
					case 4 -> printFilesAndFoldersByFirstLetter(initialPath);
				}
			}
		} else {
			log.error("Given path: \"" + initialPath + "\" is invalid.");
		}
	}

	private void searchForTheFileWithMaxOccurrencesOfTheLetterSInName(Path path) {
		Optional<Path> optionalPath = DiskAnalyzer.searchForTheFileWithMaxOccurrencesOfTheLetter(
				path,
				's');
		if (optionalPath.isPresent()) {
			System.out.println("File with the maximum number of occurrences of letter 's' in it's name: "
					+ optionalPath.get().getFileName().toAbsolutePath());
		} else {
			System.out.println("No files with letter 's' in the name were found.");
		}
	}

	private void printTop5LargestFilesBySizeInBytes(Path path) {
		List<Path> biggestFiles = DiskAnalyzer.getBiggestFilesBySize(
				path,
				5);
		System.out.println("Top 5 biggest files in given directory: ");
		biggestFiles.forEach(file -> {
			try {
				System.out.println(file.getFileName() +
						" - " + Files.readAttributes(file, BasicFileAttributes.class).size() + " bytes");
			} catch (IOException e) {
				log.error("Can't read attributes of file " + file.getFileName());
			}
		});
	}

	private void printAverageFileSizeInSpecifiedDirectoryAndItsSubdirectories(Path path) {
		long averageFileSizeInBytes = (long) DiskAnalyzer.getAverageFileSizeInBytes(path);
		System.out.println("Average size of files in given directory and its subdirectories: "
				+ averageFileSizeInBytes + " bytes.");
	}

	private void printFilesAndFoldersByFirstLetter(Path path) {
		for (char letter = 'a'; letter <= 'z'; ++letter) {
			countFilesAndFoldersByLetter(path, letter);
		}
		for (char letter = 'A'; letter <= 'Z'; ++letter) {
			countFilesAndFoldersByLetter(path, letter);
		}
	}

	private void countFilesAndFoldersByLetter(Path path, char letter) {
		long filesCount = DiskAnalyzer.getNumberOfFilesStartingWithLetter(path, letter);
		long directoriesCount = DiskAnalyzer.getNumberOfFoldersStartingWithLetter(path, letter);
		if (filesCount != 0 || directoriesCount != 0) {
			System.out.println("Letter " + letter + ": " + filesCount + " files, " + directoriesCount + " folders");
		}
	}
}
