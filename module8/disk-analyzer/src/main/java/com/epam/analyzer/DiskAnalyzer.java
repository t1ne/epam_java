package com.epam.analyzer;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class DiskAnalyzer {
    public static Optional<Path> searchForTheFileWithMaxOccurrencesOfTheLetter(Path path, char letter) {
        Stream<Path> filesWithLetterInName = findAllFilesByCriteria(path,
                (file, attributes) -> file.getFileName().toString().contains(String.valueOf(letter)));

        return filesWithLetterInName.max((path1, path2) -> {
            long firstPathCharactersMathCount = path1.getFileName().toString()
                    .chars()
                    .filter(character -> character == letter)
                    .count();
            long secondPathCharactersMathCount = path2.getFileName().toString()
                    .chars()
                    .filter(character -> character == letter)
                    .count();

            return Long.compare(firstPathCharactersMathCount, secondPathCharactersMathCount);
        });
    }

    public static List<Path> getBiggestFilesBySize(Path path, int filesCount) {
        return getAllRootLevelFilesInPath(path).stream()
                .sorted(((Comparator<Path>) (firstFile, secondFile) -> {
                    try {
                        long firstFileSIze = Files.readAttributes(firstFile, BasicFileAttributes.class).size();
                        long secondFileSize = Files.readAttributes(secondFile, BasicFileAttributes.class).size();
                        return Long.compare(firstFileSIze, secondFileSize);
                    } catch (IOException e) {
                        log.error("Can't read attributes of files: "
                                + firstFile.getFileName() + ", " + secondFile.getFileName());
                    }
                    return 0;
                }).reversed())
                .limit(filesCount)
                .collect(Collectors.toList());
    }

    public static double getAverageFileSizeInBytes(Path path) {
        return getAllFilesInPath(path).stream()
                .mapToLong(file -> {
                    try {
                        return Files.readAttributes(file, BasicFileAttributes.class).size();
                    } catch (IOException e) {
                        log.error("Can't read attributes of file " + file.getFileName());
                    }
                    return 0L;
                })
                .average()
                .orElse(0.0);
    }

    public static long getNumberOfFilesStartingWithLetter(Path path, char letter) {
        Stream<Path> filesStartingWithLetter = findAllFilesByCriteria(path,
                (file, attributes) -> file.getFileName().toString().startsWith(String.valueOf(letter)));

        return filesStartingWithLetter.filter(Files::isRegularFile).count();
    }

    public static long getNumberOfFoldersStartingWithLetter(Path path, char letter) {
        Stream<Path> filesStartingWithLetter = findAllFilesByCriteria(path,
                (file, attributes) -> file.getFileName().toString().startsWith(String.valueOf(letter)));

        return filesStartingWithLetter.filter(Files::isDirectory).count();
    }

    private static Stream<Path> findAllFilesByCriteria(Path rootPath, BiPredicate<Path, BasicFileAttributes> predicate) {
        try {
            return Files.find(
                    rootPath,
                    Integer.MAX_VALUE,
                    predicate).filter(foundPath -> foundPath != rootPath);
        } catch (IOException e) {
            log.error("Can't perform search on directory " + rootPath.getFileName());
        }
        return Stream.empty();
    }

    private static List<Path> getAllFilesInPath(Path path) {
        try {
            return Files.walk(path)
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            log.error("Can't list all files in directory" + path.getFileName());
        }
        return Collections.emptyList();
    }

    private static List<Path> getAllRootLevelFilesInPath(Path path) {
        try {
            return Files.list(path)
                    .filter(Files::isRegularFile)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            log.error("Can't list all files in directory" + path.getFileName());
        }
        return Collections.emptyList();
    }
}
