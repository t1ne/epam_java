package com.epam;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.nio.file.Files;
import java.nio.file.Path;

@SpringBootApplication
@Slf4j
public class FileMoverApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(FileMoverApplication.class, args);
	}

	@Override
	public void run(String... args) {
		if (args.length != 2) {
			log.error("""
					Missing two mandatory launch parameters:\s
					1 - Path of file to be moved
					2 - Target directory""");
		}
		Path initialPath = Path.of(args[0]);
		Path targetPath = Path.of(args[1]);
		if (Files.exists(initialPath) && Files.exists(targetPath)) {
			FastFileMover.moveViaNIOFilesAPI(args[0], args[1]);
		} else {
			log.error("Given path: \"" + initialPath + "\"; \"" +  targetPath + "\" is invalid.");
		}
	}
}
