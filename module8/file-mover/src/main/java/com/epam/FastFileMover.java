package com.epam;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

@Slf4j
public class FastFileMover {
    public static void moveViaFileStream(String from, String to) {
        File source = new File(from);
        File target = new File(to + "/" + source.getName());
        try (InputStream in = new FileInputStream(source);
                OutputStream out = new FileOutputStream(target)) {
            byte[] tempArray = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(tempArray)) > 0) {
                out.write(tempArray, 0, lengthRead);
                out.flush();
            }
        } catch (IOException e) {
            log.error("Error occurred while copying the file: " + e.getMessage());
        }
    }

    public static void moveViaBufferedFileStream(String from, String to) {
        final int BUFFER_SIZE = 102400;
        File source = new File(from);
        File target = new File(to + "/" + source.getName());
        try (InputStream in = new BufferedInputStream(new FileInputStream(source), BUFFER_SIZE);
             OutputStream out = new BufferedOutputStream(new FileOutputStream(target), BUFFER_SIZE)) {
            byte[] tempArray = new byte[BUFFER_SIZE];
            int lengthRead;
            while ((lengthRead = in.read(tempArray)) > 0) {
                out.write(tempArray, 0, lengthRead);
                out.flush();
            }
        } catch (IOException e) {
            log.error("Error occurred while copying the file: " + e.getMessage());
        }
    }

    public static void moveViaFileChannel(String from, String to) {
        File source = new File(from);
        File target = new File(to + "/" + source.getName());
        try (FileChannel sourceChannel = new FileInputStream(source).getChannel();
             FileChannel targetChannel = new FileOutputStream(target).getChannel()) {
            targetChannel.transferFrom(sourceChannel, 0, sourceChannel.size());
        } catch (IOException e) {
            log.error("Error occurred while copying the file: " + e.getMessage());
        }
    }

    public static void moveViaNIOFilesAPI(String from, String to) {
        Path source = Path.of(from);
        Path target = Path.of(to + "/" + source.getFileName());
        try {
            Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("Error occurred while copying the file: " + e.getMessage());
        }
    }
}
