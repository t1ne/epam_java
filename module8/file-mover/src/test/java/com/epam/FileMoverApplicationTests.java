package com.epam;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;

public class FileMoverApplicationTests {
	@State(Scope.Benchmark)
	public static class ExecutionPlan {

		@Param({"100"})
		public int iterations;

		@Param({"C:/test/from/test.txt",
				"C:/test/from/test1kb.txt",
				"C:/test/from/test100kb.txt",
				"C:/test/from/test10mb.txt"})
//				"C:/test/from/test1gb.txt"})
		public String from;

		@Param({"C:/test/to"})
		public String to;
	}

	@Benchmark
	@Fork(value = 1, warmups = 1)
	@BenchmarkMode(Mode.AverageTime)
	public void moveViaFileStream(ExecutionPlan executionPlan) {
		for (int i = executionPlan.iterations; i > 0; --i) {
			FastFileMover.moveViaFileStream(executionPlan.from, executionPlan.to);
		}
	}

	@Benchmark
	@Fork(value = 1, warmups = 1)
	@BenchmarkMode(Mode.AverageTime)
	public void moveViaBufferedFileStream(ExecutionPlan executionPlan) {
		for (int i = executionPlan.iterations; i > 0; --i) {
			FastFileMover.moveViaBufferedFileStream(executionPlan.from, executionPlan.to);
		}
	}

	@Benchmark
	@Fork(value = 1, warmups = 1)
	@BenchmarkMode(Mode.AverageTime)
	public void moveViaFileChannel(ExecutionPlan executionPlan) {
		for (int i = executionPlan.iterations; i > 0; --i) {
			FastFileMover.moveViaFileChannel(executionPlan.from, executionPlan.to);
		}
	}

	@Benchmark
	@Fork(value = 1, warmups = 1)
	@BenchmarkMode(Mode.AverageTime)
	public void moveViaFilesAPI(ExecutionPlan executionPlan) {
		for (int i = executionPlan.iterations; i > 0; --i) {
			FastFileMover.moveViaNIOFilesAPI(executionPlan.from, executionPlan.to);
		}
	}
}
