**Task 3 - FastFileMover**

Cost: 30 points.

Write several versions of the FastFileMover utility, which moves a file from one directory to another directory. It takes both file paths as a command line parameters. All exceptions must be handled correctly.

Required functionality:

* (5 points) It uses FileStreams
* (5 points) It uses FileStreams with buffer 100 kb
* (5 points) It uses FileChannel
* (5 points) It uses NIO 2 File API

After that prepare a performance report based on next requirements.

Measure the time for copying, run on several reference files of different sizes (1 Kb, 100 Kb, 10 Mb, 1 GB). On each file, run 1000 times, get the average time.

**Benchmarking results**

*1gb file test was excluded due to long measurement time spent on testing. For the same reasons, smaller number of iterations were used, 100 instead of 1000.*
````                                                             (from)  (iterations)        (to)  Mode  Cnt  Score   Error  Units
FileMoverApplicationTests.moveViaBufferedFileStream       C:/test/from/test.txt           100  C:/test/to  avgt    5  0.034 ± 0.002   s/op
FileMoverApplicationTests.moveViaBufferedFileStream    C:/test/from/test1kb.txt           100  C:/test/to  avgt    5  0.039 ± 0.008   s/op
FileMoverApplicationTests.moveViaBufferedFileStream  C:/test/from/test100kb.txt           100  C:/test/to  avgt    5  0.047 ± 0.003   s/op
FileMoverApplicationTests.moveViaBufferedFileStream   C:/test/from/test10mb.txt           100  C:/test/to  avgt    5  1.071 ± 0.153   s/op
FileMoverApplicationTests.moveViaFileChannel              C:/test/from/test.txt           100  C:/test/to  avgt    5  0.051 ± 0.011   s/op
FileMoverApplicationTests.moveViaFileChannel           C:/test/from/test1kb.txt           100  C:/test/to  avgt    5  0.052 ± 0.005   s/op
FileMoverApplicationTests.moveViaFileChannel         C:/test/from/test100kb.txt           100  C:/test/to  avgt    5  0.059 ± 0.016   s/op
FileMoverApplicationTests.moveViaFileChannel          C:/test/from/test10mb.txt           100  C:/test/to  avgt    5  0.943 ± 0.128   s/op
FileMoverApplicationTests.moveViaFileStream               C:/test/from/test.txt           100  C:/test/to  avgt    5  0.046 ± 0.006   s/op
FileMoverApplicationTests.moveViaFileStream            C:/test/from/test1kb.txt           100  C:/test/to  avgt    5  0.036 ± 0.001   s/op
FileMoverApplicationTests.moveViaFileStream          C:/test/from/test100kb.txt           100  C:/test/to  avgt    5  0.095 ± 0.004   s/op
FileMoverApplicationTests.moveViaFileStream           C:/test/from/test10mb.txt           100  C:/test/to  avgt    5  6.585 ± 0.434   s/op
FileMoverApplicationTests.moveViaFilesAPI                 C:/test/from/test.txt           100  C:/test/to  avgt    5  0.082 ± 0.010   s/op
FileMoverApplicationTests.moveViaFilesAPI              C:/test/from/test1kb.txt           100  C:/test/to  avgt    5  0.089 ± 0.014   s/op
FileMoverApplicationTests.moveViaFilesAPI            C:/test/from/test100kb.txt           100  C:/test/to  avgt    5  0.093 ± 0.011   s/op
FileMoverApplicationTests.moveViaFilesAPI             C:/test/from/test10mb.txt           100  C:/test/to  avgt    5  1.009 ± 0.042   s/op