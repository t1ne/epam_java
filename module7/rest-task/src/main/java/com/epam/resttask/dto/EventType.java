package com.epam.resttask.dto;

public enum EventType {
    CONFERENCE,
    RACE,
    SHOW,
    PERFORMANCE,
    STANDUP,
    LECTURE
}
