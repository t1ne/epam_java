package com.epam.resttask.controller;

import com.epam.resttask.dto.Event;
import com.epam.resttask.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/events")
public class EventController {
    @Autowired
    private EventService eventService;

    @GetMapping
    public List<Event> getAllEvents(@RequestParam(required = false) Optional<String> title) {
        return  (title.isPresent()) ? eventService.getAllEventsByTitle(title.get()) : eventService.getAllEvents();
    }

    @GetMapping("/{id}")
    public Event getEventById(@PathVariable long id) {
        return eventService.getEvent(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Event create(@RequestBody Event event) {
        return eventService.createEvent(event);
    }

    @PutMapping("/{id}")
    public void update(@PathVariable long id, @RequestBody Event event) {
        Event eventById = eventService.getEvent(id);
        eventById.setTitle(event.getTitle());
        eventById.setEventType(event.getEventType());
        eventById.setDateTime(event.getDateTime());
        eventById.setPlace(event.getPlace());
        eventById.setSpeaker(event.getSpeaker());
        eventService.updateEvent(eventById);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id) {
        eventService.deleteEvent(id);
    }
}
