package com.epam.resttask.service;

import com.epam.resttask.dto.Event;
import com.epam.resttask.dao.EventRepository;
import com.epam.resttask.exception.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EventServiceImpl implements EventService {

    @Autowired
    private EventRepository repository;

    @Override
    public Event createEvent(Event event) {
        return repository.save(event);
    }

    @Override
    public Event updateEvent(Event event) {
        return repository.save(event);
    }

    @Override
    public Event getEvent(Long id) {
        return repository.findById(id).orElseThrow(
                () -> new NotFoundException("Event with ID" + id + " wasn't found."));
    }

    @Override
    public void deleteEvent(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<Event> getAllEvents() {
        return repository.findAll();
    }

    @Override
    public List<Event> getAllEventsByTitle(String title) {
        return repository.findByTitle(title);
    }
}
