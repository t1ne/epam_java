Task 2 - Task Manager with Mongo DB

Cost: 30 points + 10 bonus points.

Install MongoDB and use corresponding Java driver.

Create simple task manager console app. Your tasks should have next fields:

date of creation;
deadline;
name;
description;
list of subtasks with simple structure (name/description);
category.
Provide next operation:

1. Display on console all tasks.
2. Display overdue tasks.
3. Display all tasks with the specific category (query parameter).
4. Display all subtasks related to tasks with the specific category (query parameter).
5. Perform insert/update/delete of the task.
6. Perform insert/update/delete all subtasks of the given task (query parameter).
7. Support full-text search by word in task description.
8. Support full-text search by sub-task name.
9. For highest mark, you can try implement DAO with any ORM solution for Mongo (+10 bonus points).