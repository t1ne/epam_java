package com.epam.model;

public enum Category {
    BUG,
    ISSUE,
    STORY,
    SPIKE;
}
