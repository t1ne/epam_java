package com.epam.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Task {
    private ObjectId id;
    private String name;
    private String description;
    private Category category;
    private Date creationDate;
    private Date dueDate;
    private List<Subtask> subtaskList;

    @Builder
    public Task(String name, String description, Category category, Date creationDate, Date dueDate, List<Subtask> subtaskList) {
        this.name = name;
        this.description = description;
        this.category = category;
        this.creationDate = creationDate;
        this.dueDate = dueDate;
        this.subtaskList = subtaskList;
    }
}
