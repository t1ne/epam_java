package com.epam.model;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Subtask {
    private String name;
    private String description;

    @Builder
    public Subtask(String name, String description) {
        this.name = name;
        this.description = description;
    }
}
