package com.epam;

import com.epam.model.Category;
import com.epam.model.Subtask;
import com.epam.model.Task;
import com.epam.mongo.TaskDAO;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        try (TaskDAO dao = new TaskDAO()) {
            populateCollection(dao);
            task1(dao);
            task2(dao);
            task3(dao);
            task4(dao);
            task5(dao);
            task6(dao);
            task7(dao);
            task8(dao);
        }
    }

    private static void populateCollection(TaskDAO dao) {
        dao.removeAllTasks();
        dao.saveTask(Task.builder()
                .name("Create bar component")
                .description("React component should be created")
                .category(Category.ISSUE)
                .creationDate(new Date())
                .dueDate(parseDateFromLocalDate(LocalDate.now().plusDays(3)))
                .build());
        dao.saveTask(Task.builder()
                .name("Fix unit test")
                .description("Fix unit test in data extraction module")
                .category(Category.BUG)
                .creationDate(new Date())
                .dueDate(parseDateFromLocalDate(LocalDate.now().plusDays(3)))
                .build());
        dao.saveTask(Task.builder()
                .name("Task with subtask")
                .description("Task with subtask")
                .category(Category.ISSUE)
                .subtaskList(Collections.singletonList(Subtask.builder()
                        .name("Subtask name")
                        .description("Just random description")
                        .build()))
                .build());
        dao.saveTask(Task.builder()
                .name("Overdue task")
                .description("Task with due date set to current minus week")
                .dueDate(parseDateFromLocalDate(LocalDate.now().minusDays(7)))
                .build());
        dao.saveTask(Task.builder()
                .name("Task with word \"meme\" in description")
                .description("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed meme do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ")
                .build());
        dao.saveTask(Task.builder()
                .name("Another task with word \"meme\" in description")
                .description("Duis aute irure dolor in reprehenderit in voluptate velit, meme, esse cillum dolore eu fugiat nulla pariatur. ")
                .build());
    }

    private static Date parseDateFromLocalDate(LocalDate localDate) {
       return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    // Display on console all tasks.
    private static void task1(TaskDAO dao) {
        System.out.println(CONSOLE_TEXT_COLOR.CYAN + "All tasks:" + CONSOLE_TEXT_COLOR.RESET);
        dao.getAllTasks().forEach(System.out::println);

        System.out.print("\n");
    }

    // Display overdue tasks.
    private static void task2(TaskDAO dao) {
        System.out.println(CONSOLE_TEXT_COLOR.RED + "Overdue tasks:" + CONSOLE_TEXT_COLOR.RESET);
        dao.getOverdueTasks().forEach(System.out::println);

        System.out.print("\n");
    }

    // Display all tasks with the specific category.
    private static void task3(TaskDAO dao) {
        final Category category = Category.BUG;
        System.out.println(CONSOLE_TEXT_COLOR.GREEN + "Tasks with " + Category.BUG + " category:" + CONSOLE_TEXT_COLOR.RESET);
        dao.getTasksByCategory(category).forEach(System.out::println);

        System.out.print("\n");
    }

    // Display all subtasks related to tasks with the specific category.
    private static void task4(TaskDAO dao) {
        final Category category = Category.ISSUE;
        System.out.println(CONSOLE_TEXT_COLOR.PURPLE + "Subtasks related to tasks with " + category + " category:" + CONSOLE_TEXT_COLOR.RESET);
        dao.getSubtasksByCategory(category).forEach(System.out::println);

        System.out.print("\n");
    }

    // Perform insert/update/delete of the task.
    private static void task5(TaskDAO dao) {
        Task randomTask = dao.getAllTasks().stream().findFirst().get();
        String taskName = randomTask.getName();

        System.out.println(CONSOLE_TEXT_COLOR.YELLOW + "Update task \"" + taskName + "\":" + CONSOLE_TEXT_COLOR.RESET);
        System.out.println(randomTask);
        String updatedDescription = "This is updated description";
        randomTask.setDescription(updatedDescription);
        dao.updateTask(randomTask);
        System.out.println(CONSOLE_TEXT_COLOR.YELLOW + "Updated task \"" + taskName + "\":" + CONSOLE_TEXT_COLOR.RESET);
        Task updatedTask = dao.getAllTasks().stream().filter(task -> task.getName().equals(taskName)).findFirst().get();
        System.out.println(updatedTask);

        dao.deleteTask(randomTask.getId());
        boolean isPresent = dao.getAllTasks().stream().anyMatch(task -> task.getName().equals(taskName));
        System.out.println(CONSOLE_TEXT_COLOR.YELLOW + "Task was deleted, search whether task is present result: " + isPresent + CONSOLE_TEXT_COLOR.RESET);

        System.out.print("\n");
    }

    // Perform insert/update/delete all subtasks of the given task (query parameter).
    private static void task6(TaskDAO dao) {
        Task randomTask = dao.getAllTasks().stream().findFirst().get();
        String taskName = randomTask.getName();

        System.out.println(CONSOLE_TEXT_COLOR.CYAN + "Add subtask for the task \"" + taskName + "\":" + CONSOLE_TEXT_COLOR.RESET);
        System.out.println(randomTask);
        Subtask newSubtask = Subtask.builder().name("New").description("subtask").build();
        dao.addSubtaskToTask(randomTask, newSubtask);
        System.out.println(CONSOLE_TEXT_COLOR.CYAN + "Subtasks list after insertion:" + CONSOLE_TEXT_COLOR.RESET);
        List<Subtask> subtaskList = dao.getAllTasks().stream().filter(task -> task.getName().equals(taskName)).findFirst().map(Task::getSubtaskList).get();
        System.out.println(subtaskList);

        System.out.println(CONSOLE_TEXT_COLOR.CYAN + "Updating subtasks for the task \"" + taskName + "\":" + CONSOLE_TEXT_COLOR.RESET);
        System.out.println(randomTask);
        dao.updateSubtasksInTheTask(randomTask, List.of(
                Subtask.builder().name("First").description("subtask").build(),
                Subtask.builder().name("Second").description("subtask").build()));
        System.out.println(CONSOLE_TEXT_COLOR.CYAN + "Subtasks list after update:" + CONSOLE_TEXT_COLOR.RESET);
        subtaskList = dao.getAllTasks().stream().filter(task -> task.getName().equals(taskName)).findFirst().map(Task::getSubtaskList).get();
        System.out.println(subtaskList);

        dao.deleteAllSubtasksFromTheTask(randomTask);
        boolean areSubtasksPresent = dao.getAllTasks().stream().filter(task -> task.getName().equals(taskName)).findFirst().get().getSubtaskList() != null;
        System.out.println(CONSOLE_TEXT_COLOR.CYAN + "Subtasks was deleted, search whether subtasks are present in the result: "
                + areSubtasksPresent + CONSOLE_TEXT_COLOR.RESET);

        System.out.print("\n");
    }

    // Full-text search by word in task description.
    private static void task7(TaskDAO dao) {
        String word = "meme";
        System.out.println(CONSOLE_TEXT_COLOR.BLUE + "All tasks with word \"" + word + "\" in description:" + CONSOLE_TEXT_COLOR.RESET);
        dao.getTasksContainingWordInDescription(word).forEach(System.out::println);

        System.out.print("\n");
    }

    // Full-text search by sub-task name.
    private static void task8(TaskDAO dao) {
        String subTaskName = "Subtask name";
        System.out.println(CONSOLE_TEXT_COLOR.RED + "All tasks with subtask with name \"" + subTaskName + "\":" + CONSOLE_TEXT_COLOR.RESET);
        dao.getTasksBySubTaskName(subTaskName).forEach(System.out::println);
        System.out.print("\n");
    }
}
