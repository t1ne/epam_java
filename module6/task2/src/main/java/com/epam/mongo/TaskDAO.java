package com.epam.mongo;

import com.epam.model.Category;
import com.epam.model.Subtask;
import com.epam.model.Task;
import com.google.common.collect.ImmutableList;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Indexes;
import org.bson.BsonNull;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;
import org.bson.types.ObjectId;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.project;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Filters.lt;
import static com.mongodb.client.model.Filters.not;
import static com.mongodb.client.model.Projections.excludeId;
import static com.mongodb.client.model.Projections.fields;
import static com.mongodb.client.model.Projections.include;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class TaskDAO implements AutoCloseable{
    private final MongoClient client;
    private final MongoCollection<Task> taskCollection;

    public TaskDAO() {
        ConnectionString connectionString = new ConnectionString(System.getProperty("mongodb.uri"));
        CodecRegistry pojoCodecRegistry = fromProviders(PojoCodecProvider.builder().automatic(true).build());
        CodecRegistry codecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(),
                fromProviders(new EnumCodecProvider()),
                pojoCodecRegistry);
        MongoClientSettings clientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .codecRegistry(codecRegistry)
                .build();
        this.client =  MongoClients.create(clientSettings);
        taskCollection = client.getDatabase("task2").getCollection("tasks", Task.class);
        taskCollection.createIndex(Indexes.compoundIndex(Indexes.text("description"), Indexes.text("subtaskList.name")));
    }

    public List<Task> getAllTasks() {
        return ImmutableList.copyOf(taskCollection.find());
    }

    public List<Task> getOverdueTasks() {
        return ImmutableList.copyOf(taskCollection.find(lt("dueDate", new Date())));
    }

    public List<Task> getTasksByCategory(Category category) {
        return ImmutableList.copyOf(taskCollection.find(eq("category", category)));
    }


    public List<Subtask> getSubtasksByCategory(Category category) {
        //mapping to subtask list is needed as the return from aggregate() is of type Iterable<Task>
        return ImmutableList.copyOf(taskCollection.aggregate(List.of(
                match(eq("category", category)),
                match(not(eq("subtaskList", BsonNull.VALUE))),
                project(fields(excludeId(), include("subtaskList"))))))
                .stream().map(Task::getSubtaskList).flatMap(List::stream).collect(Collectors.toList());
    }

    public void saveTask(Task task) {
        taskCollection.insertOne(task);
    }

    public void updateTask(Task updatedTask) {
        Document filterByGradeId = new Document("_id", updatedTask.getId());
        taskCollection.findOneAndReplace(filterByGradeId, updatedTask);
    }

    public void deleteTask(ObjectId objectId) {
        taskCollection.deleteOne(new Document("_id", objectId));
    }

    public void addSubtaskToTask(Task task, Subtask subtask) {
        List<Subtask> subtasks = task.getSubtaskList();
        if (subtasks != null)  {
            subtasks.add(subtask);
        } else {
            task.setSubtaskList(Collections.singletonList(subtask));
        }
        updateTask(task);
    }

    public void updateSubtasksInTheTask(Task task, List<Subtask> subtasks) {
        task.setSubtaskList(subtasks);
        updateTask(task);
    }

    public void deleteAllSubtasksFromTheTask(Task task) {
        task.setSubtaskList(null);
        updateTask(task);
    }

    public List<Task> getTasksContainingWordInDescription(String word) {
        return ImmutableList.copyOf(taskCollection.find(new Document("$text", new Document("$search", word))));
    }

    public List<Task> getTasksBySubTaskName(String name) {
        return ImmutableList.copyOf(taskCollection.find(new Document("$text", new Document("$search", name))));
    }

    public void removeAllTasks() {
        taskCollection.deleteMany(new Document());
    }

    @Override
    public void close(){
        client.close();
    }
}
