package com.epam.mongo;

import com.epam.model.Category;
import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecProvider;
import org.bson.codecs.configuration.CodecRegistry;

public class EnumCodecProvider implements CodecProvider {
    @Override
    public <T> Codec<T> get(Class<T> clazz, CodecRegistry registry) {
        if (clazz == Category.class) {
            //noinspection unchecked
            return (Codec<T>) new CategoryCodec();
        }
        return null;
    }

}