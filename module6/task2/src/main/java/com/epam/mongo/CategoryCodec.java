package com.epam.mongo;

import com.epam.model.Category;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.EncoderContext;

public class CategoryCodec implements Codec<Category> {
    @Override
    public Category decode(BsonReader reader, DecoderContext decoderContext) {
        return Category.valueOf(reader.readString());
    }

    @Override
    public void encode(BsonWriter writer, Category value, EncoderContext encoderContext) {
        writer.writeString(value.name());
    }

    @Override
    public Class<Category> getEncoderClass() {
        return Category.class;
    }
}
