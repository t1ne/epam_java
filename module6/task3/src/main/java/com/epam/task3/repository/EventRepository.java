package com.epam.task3.repository;

import com.epam.task3.model.Event;
import org.springframework.data.repository.CrudRepository;

public interface EventRepository extends CrudRepository<Event, Long> {}
