package com.epam.task3;

import com.epam.task3.model.Event;
import com.epam.task3.repository.EventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;
import java.util.Random;

@SpringBootApplication
public class Task3Application implements CommandLineRunner {
	@Autowired private EventRepository eventRepository;

	public static void main(String[] args) {
		SpringApplication.run(Task3Application.class, args);
	}

	@Override
	public void run(String... args) {
		insert();
		retrieve();
		for (long i = 0; i < 10000; ++i) {
			retrieveByRandomId();
		}
		for (long i = 0; i < 10000; ++i) {
			deleteRandom();
		}
		insert();
		retrieve();
	}

	private void insert() {
		for (long i = 0; i < 10000000; ++ i) {
			eventRepository.save(Event.builder().id(i).name(i + "").date(new Date()).build());
		}
	}

	private void retrieve() {
		eventRepository.findAll();
	}

	private void retrieveByRandomId() {
		long id = new Random().nextLong();
		eventRepository.findById(id);
	}

	private void deleteRandom() {
		long id = new Random().nextLong();
		eventRepository.deleteById(id);
	}
}
