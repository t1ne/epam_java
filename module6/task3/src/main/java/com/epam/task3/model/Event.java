package com.epam.task3.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import java.util.Date;
import java.util.UUID;

@Data
@Builder
@Table("event")
public class Event {
    @PrimaryKey
    private Long id;

    private String name;

    private Date date;
}
