#!/bin/bash

# Run the first node and keep it in background up and running
docker run --name cassandra-1-3-0 -p 9042:9042 -d cassandra:3.0
INSTANCE1=$(docker inspect --format='{{ .NetworkSettings.IPAddress }}' cassandra-1-3-0)
echo "Instance 1: ${INSTANCE1}"

# Run the second node
docker run --name cassandra-2-3-0 -p 9043:9042 -d -e CASSANDRA_SEEDS=$INSTANCE1 cassandra:3.0
INSTANCE2=$(docker inspect --format='{{ .NetworkSettings.IPAddress }}' cassandra-2-3-0)
echo "Instance 2: ${INSTANCE2}"

# Connect to the cluster using cqlsh
# It will delete the docker container automatically once you close it
docker run -it --link cassandra-1-3-0 --rm cassandra:3.0 bash -c "exec cqlsh $INSTANCE1"

# Cleanup
# docker stop cassandra-1-3-0
# docker stop cassandra-2-3-0
# docker rm cassandra-1-3-0
# docker rm cassandra-2-3-0

docker run --name cas1 -p 9042:9042 -e CASSANDRA_CLUSTER_NAME=MyCluster -e CASSANDRA_ENDPOINT_SNITCH=GossipingPropertyFileSnitch -e CASSANDRA_DC=datacenter1 -d cassandra
docker run --name cas2 -e CASSANDRA_SEEDS="172.17.0.2" -e CASSANDRA_CLUSTER_NAME=MyCluster -e CASSANDRA_ENDPOINT_SNITCH=GossipingPropertyFileSnitch -e CASSANDRA_DC=datacenter1 -d cassandra
docker run --name cas3 -e CASSANDRA_SEEDS="172.17.0.2" -e CASSANDRA_CLUSTER_NAME=MyCluster -e CASSANDRA_ENDPOINT_SNITCH=GossipingPropertyFileSnitch -e CASSANDRA_DC=datacenter1 -d cassandra
docker run --name cas4 -e CASSANDRA_SEEDS="172.17.0.2" -e CASSANDRA_CLUSTER_NAME=MyCluster -e CASSANDRA_ENDPOINT_SNITCH=GossipingPropertyFileSnitch -e CASSANDRA_DC=datacenter1 -d cassandra

CREATE KEYSPACE mykeyspace
WITH replication = {
	'class' : 'NetworkTopologyStrategy',
	'dc1' : 1,
	'dc2' : 1
};
CREATE TABLE mykeyspace.mytable (
	id int primary key,
	name text
);