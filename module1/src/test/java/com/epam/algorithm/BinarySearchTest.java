package com.epam.algorithm;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class BinarySearchTest {

    private static Stream<Arguments> generator() {
        return Stream.of(
                Arguments.of(new IterativeBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), 1, 0),
                Arguments.of(new RecursiveBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), 1, 0),
                Arguments.of(new IterativeBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), 4, 3),
                Arguments.of(new RecursiveBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), 4, 3),
                Arguments.of(new IterativeBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), 7, 6),
                Arguments.of(new RecursiveBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), 7, 6),
                Arguments.of(new IterativeBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), 1, 0),
                Arguments.of(new RecursiveBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), 1, 0),
                Arguments.of(new IterativeBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), 4, 3),
                Arguments.of(new RecursiveBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), 4, 3),
                Arguments.of(new IterativeBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), 7, 6),
                Arguments.of(new RecursiveBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), 7, 6)
        );
    }

    private static Stream<Arguments> emptyArrayGenerator() {
        return Stream.of(
                Arguments.of(new IterativeBinarySearch(), Collections.emptyList()),
                Arguments.of(new RecursiveBinarySearch(), Collections.emptyList())
        );
    }

    private static Stream<Arguments> notPresentValueGenerator() {
        return Stream.of(
                Arguments.of(new IterativeBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), -1),
                Arguments.of(new RecursiveBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), -1),
                Arguments.of(new IterativeBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), 12),
                Arguments.of(new RecursiveBinarySearch(), new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), 12),
                Arguments.of(new IterativeBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), -1),
                Arguments.of(new RecursiveBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), -1),
                Arguments.of(new IterativeBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), 12),
                Arguments.of(new RecursiveBinarySearch(), new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), 12)
        );
    }

    @ParameterizedTest(name = "{index}: {0}, {1}, array: {2}, searched value: {3}")
    @MethodSource("generator")
    void test_search(BinarySearch search, SortingStrategy strategy, List<Integer> inputArray, Integer searchedValue, Integer expectedIndex) {
        search.setSortingStrategy(strategy);
        int index = search.search(inputArray, searchedValue);
        assertEquals(expectedIndex, index);
        assertEquals(searchedValue, inputArray.get(index));
    }

    @ParameterizedTest(name = "{index}: {0}, empty array")
    @MethodSource("emptyArrayGenerator")
    void test_empty_array(BinarySearch search, List<Integer> inputArray) {
        int index = search.search(inputArray, 1);
        assertEquals(-1, index);
    }

    @ParameterizedTest(name = "{index}: {0}, {1}, array: {2}, not present searched value: {3}")
    @MethodSource("notPresentValueGenerator")
    void test_not_present_value(BinarySearch search, SortingStrategy strategy, List<Integer> inputArray, Integer notPresentSearchedValue) {
        search.setSortingStrategy(strategy);
        int index = search.search(inputArray, notPresentSearchedValue);
        assertEquals(-1, index);
    }
}