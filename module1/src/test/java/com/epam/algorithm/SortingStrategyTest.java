package com.epam.algorithm;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class SortingStrategyTest {
    private static Stream<Arguments> generator() {
        return Stream.of(
                Arguments.of(new MergeSort(), List.of(4, 3, 7, 2, 1, 7, 6, 0, 5), List.of(0, 1, 2, 3, 4, 5, 6, 7, 7)),
                Arguments.of(new InsertionSort(), List.of(4, 3, 7,  2, 1, 7, 6, 0, 5), List.of(0, 1, 2, 3, 4, 5, 6, 7, 7)),
                Arguments.of(new MergeSort(), List.of(7, 6, 5, 4, 3, 2, 1, 1), List.of(1, 1, 2, 3, 4, 5, 6, 7)),
                Arguments.of(new InsertionSort(), List.of(7, 6, 5, 4, 3, 2, 1, 1), List.of(1, 1, 2, 3, 4, 5, 6, 7)),
                Arguments.of(new MergeSort(), List.of(1, 2, 3, 4, 5, 6, 7), List.of(1, 2, 3, 4, 5, 6, 7)),
                Arguments.of(new InsertionSort(), List.of(1, 2, 3, 4, 5, 6, 7), List.of(1, 2, 3, 4, 5, 6, 7)),
                Arguments.of(new MergeSort(), Collections.emptyList(), Collections.emptyList()),
                Arguments.of(new InsertionSort(), Collections.emptyList(), Collections.emptyList())
        );
    }

    @ParameterizedTest(name = "{index} - {0}: input array: {1}, expected array: {2}")
    @MethodSource("generator")
    void test_sort(SortingStrategy strategy, List<Integer> input, List<Integer> sorted) {
        assertEquals(sorted, strategy.sort(input));
    }
}