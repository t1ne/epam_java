package com.epam.algorithm.cache;

import com.google.common.cache.CacheStats;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GuavaCacheServiceTest {
    private final GuavaCacheService cacheService = new GuavaCacheService();

    @AfterEach
    public void afterEach() {
        CacheStats stats = cacheService.getStats();
        System.out.println("Average  time spent for putting new values into cache: " + stats.averageLoadPenalty());
        System.out.println("Number of cache evictions: " + stats.evictionCount());
        cacheService.cleanUp();
    }

    @Test
    public void whenValueIsPutSameValueShouldBeReturnedTest() {
        CacheEntry entry1 = new CacheEntry("1");
        CacheEntry entry2 = new CacheEntry("2");
        cacheService.put(entry1);
        cacheService.put(entry2);
        assertEquals(entry1, cacheService.get("1"));
        assertEquals(entry2, cacheService.get("2"));
    }

    @Test
    public void whenRetrievedForTheFirstTimeValueShouldBePutTest() {
        CacheEntry entry = cacheService.get("1");
        assertEquals(entry, cacheService.get("1"));
    }

    @Test
    public void whenInputIsBiggerThanMaxSizeThenOverheadItemShouldBeEvictedTest() {
        for (int i = 0; i < 100000; ++i) {
            cacheService.put(new CacheEntry(String.valueOf(i)));
        }
    }

    @Test
    public void evictedByTimeoutTest() throws InterruptedException {
        cacheService.put(new CacheEntry("1"));
        Thread.sleep(6000);
    }
}