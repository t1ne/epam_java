package com.epam.algorithm.benchmark;

import com.epam.algorithm.InsertionSort;
import com.epam.algorithm.MergeSort;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Warmup;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 10, time=500, timeUnit=TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time=500, timeUnit= TimeUnit.MILLISECONDS)
@Fork(1)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
public class SortBenchmarkTest {
    @Benchmark
    public void benchmark_array_insertion_sort(ArrayContainer d) {
        new InsertionSort().sort(d.getArrayToSort());
    }

    @Benchmark
    public void benchmark_array_merge_sort(ArrayContainer d) {
        new MergeSort().sort(d.getArrayToSort());
    }
}
