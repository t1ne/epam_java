package com.epam.algorithm.benchmark;

import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Param;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@State(Scope.Benchmark)
public class ArrayContainer {

    @Param({"10", "50", "100", "500", "1000", "10000", "100000"})
    int arraySize;

    // initial unsorted array
    List<Integer> suffledArray;
    // system date copy
    List<Integer> arrayToSort;

    @Setup(Level.Trial)
    public void initArray() {
        // create a shuffled array of int
        suffledArray = new ArrayList<>(arraySize);
        for (int i = 0; i < arraySize; i++) {
            suffledArray.add(new Random().nextInt(1000));
        }
    }

    @Setup(Level.Invocation)
    public void makeArrayCopy() {
        // copy shuffled array to reinit the array to sort
        arrayToSort = new ArrayList<>(suffledArray);
    }

    List<Integer> getArrayToSort() {
        return arrayToSort;
    }
}