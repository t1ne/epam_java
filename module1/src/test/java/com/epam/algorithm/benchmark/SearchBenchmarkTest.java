package com.epam.algorithm.benchmark;

import com.epam.algorithm.IterativeBinarySearch;
import com.epam.algorithm.RecursiveBinarySearch;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Warmup;

import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@Warmup(iterations = 10, time=500, timeUnit= TimeUnit.MILLISECONDS)
@Measurement(iterations = 10, time=500, timeUnit= TimeUnit.MILLISECONDS)
@Fork(1)
@OutputTimeUnit(TimeUnit.MICROSECONDS)
public class SearchBenchmarkTest {
    @Benchmark
    public void benchmark_array_binary_search_iterative(ArrayContainer d) {
        new IterativeBinarySearch().search(d.getArrayToSort(), new Random().nextInt(1000));
    }

    @Benchmark
    public void benchmark_array_binary_search_recursive(ArrayContainer d) {
        new RecursiveBinarySearch().search(d.getArrayToSort(), new Random().nextInt(1000));
    }
}
