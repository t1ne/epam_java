package com.epam.algorithm.btree;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TreeTraversalTest {
    private Node root;

    @BeforeEach
    void setUp() {
        /*
                6
               / \
              4   8
             / \   \
            1   5   9
         */
        root = new Node(6,
                new Node(4,
                        new Node(1), new Node(5)),
                new Node(8,
                        null, new Node(9)));
    }

    @Test
    void test_traverseInOrder() {
        List<Integer> nodes = new LinkedList<>();
        TreeTraversal.traverseInOrder(root, nodes);
        assertEquals(List.of(1, 4, 5, 6, 8, 9), nodes);
    }

    @Test
    void test_traversePreOrder() {
        List<Integer> nodes = new LinkedList<>();
        TreeTraversal.traversePreOrder(root, nodes);
        assertEquals(List.of(6, 4, 1, 5, 8, 9), nodes);
    }

    @Test
    void test_traversePostOrder() {
        List<Integer> nodes = new LinkedList<>();
        TreeTraversal.traversePostOrder(root, nodes);
        assertEquals(List.of(1, 5, 4, 9, 8, 6), nodes);
    }

    @Test
    void test_traverseLevelOrder() {
        List<Integer> nodes = new LinkedList<>();
        TreeTraversal.traverseLevelOrder(root, nodes);
        assertEquals(List.of(6, 4, 8, 1, 5, 9), nodes);
    }
}