package com.epam.algorithm.btree;

/**
 * Class which is used to represent binary tree structure (node with left and right leaves).
 */
public class Node {
    public int value;
    public Node left;
    public Node right;

    /**
     * Class constructor for constructing leaf (node without children).
     */
    public Node(int value) {
        this.value = value;
        this.left = null;
        this.right = null;
    }

    /**
     * Class constructor for constructing node with left and|or right children nodes.
     */
    public Node(int value, Node left, Node right) {
        this.value = value;
        this.left = left;
        this.right = right;
    }
}
