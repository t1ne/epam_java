package com.epam.algorithm.btree;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * Util tree traversal class which allows to traverse tree using different traversal algorithms
 */
public class TreeTraversal {
    /**
     * Returns traversal result via input parameter
     * @param  node  root node of btree
     * @param  vals  traversal result, input list is modified
     */
    public static void traverseInOrder(Node node, List<Integer> vals) {
        if (node != null) {
            traverseInOrder(node.left, vals);
            vals.add(node.value);
            traverseInOrder(node.right, vals);
        }
    }

    /**
     * Returns traversal result via input parameter
     * @param  node  root node of btree
     * @param  vals  traversal result, input list is modified
     */
    public static void traversePreOrder(Node node, List<Integer> vals) {
        if (node != null) {
            vals.add(node.value);
            traversePreOrder(node.left, vals);
            traversePreOrder(node.right, vals);
        }
    }

    /**
     * Returns traversal result via input parameter
     * @param  node  root node of btree
     * @param  vals  traversal result, input list is modified
     */
    public static void traversePostOrder(Node node, List<Integer> vals) {
        if (node != null) {
            traversePostOrder(node.left, vals);
            traversePostOrder(node.right, vals);
            vals.add(node.value);
        }
    }

    /**
     * Returns traversal result via input parameter
     * @param  root  root node of btree
     * @param  vals  traversal result, input list is modified
     */
    public static void traverseLevelOrder(Node root, List<Integer> vals) {
        if (root == null) {
            return;
        }
        Queue<Node> nodes = new LinkedList<>();
        nodes.add(root);
        while (!nodes.isEmpty()) {
            Node node = nodes.remove();
            vals.add(node.value);
            if (node.left != null) {
                nodes.add(node.left);
            }
            if (node.right != null) {
                nodes.add(node.right);
            }
        }
    }
}
