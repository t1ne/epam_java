package com.epam.algorithm.cache;

public interface CacheService {
    CacheEntry get(String key);

    void put(CacheEntry entry);
}
