package com.epam.algorithm.cache;

import lombok.extern.java.Log;

import java.text.MessageFormat;

@Log
public class RemovalListener {
    public void invoke(String key, String cause) {
        log.info(MessageFormat.format("{0} was evicted, cause: {1}", key, cause));
    }
}
