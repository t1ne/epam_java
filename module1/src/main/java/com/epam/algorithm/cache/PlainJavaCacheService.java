package com.epam.algorithm.cache;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.stream.Collectors;

public class PlainJavaCacheService implements CacheService {
    private final List<Long> timeSpentOnPut = new LinkedList<>();
    private int evictionsCount = 0;

    private final transient HashMap<String, CacheEntry> cache = new HashMap<>(INITIAL_CAPACITY);
    private final transient HashMap<String, Integer> cacheHits = new HashMap<>(INITIAL_CAPACITY);
    private final transient HashMap<String, Boolean> wasAccessed = new HashMap<>(INITIAL_CAPACITY);

    private final RemovalListener removalLister = new RemovalListener();

    private static final int INITIAL_CAPACITY = 100;
    private static final int MAX_CAPACITY = 100000;
    private static final long TIME_POLICY = 5000L;

    public PlainJavaCacheService() {
        TimerTask notAccessedEvictionPolicy = new TimerTask() {
            public void run() {
                evictNotAccessedEntriesByTimePolicy();
            }
        };
        new Timer().schedule(notAccessedEvictionPolicy, 0L, TIME_POLICY);
    }

    @Override
    public synchronized CacheEntry get(String key) {
        if (cache.containsKey(key)) {
            cacheHits.put(key, cacheHits.get(key) + 1);
            wasAccessed.put(key, true);
            return cache.get(key);
        } else {
            CacheEntry newEntry = new CacheEntry(key);
            put(newEntry);
            return newEntry;
        }
    }

    @Override
    public synchronized void put(CacheEntry entry) {
        long start = System.currentTimeMillis();
        if (cache.size() >= MAX_CAPACITY) {
            evictLFU();
        }
        cache.put(entry.getValue(), entry);
        cacheHits.put(entry.getValue(), 1);
        wasAccessed.put(entry.getValue(), true);
        long finish = System.currentTimeMillis();
        long timeElapsed = finish - start;
        timeSpentOnPut.add(timeElapsed);
    }

    private synchronized void evictLFU() {
        Map.Entry<String, Integer> min = Collections.min(cacheHits.entrySet(), Map.Entry.comparingByValue());
        removalLister.invoke(min.getKey(), "MAX_SIZE");
        evictionsCount++;
        cache.remove(min.getKey());
    }

    private synchronized void evictNotAccessedEntriesByTimePolicy() {
        Set<String> notAccessedEntriesKeys =
                cache.keySet().stream()
                        .filter(entryKey -> !wasAccessed.get(entryKey))
                        .collect(Collectors.toSet());
        evictionsCount += notAccessedEntriesKeys.size();
        notAccessedEntriesKeys.forEach(cache::remove);
        notAccessedEntriesKeys.forEach(key -> removalLister.invoke(key, "TIME_POLICY"));
        wasAccessed.entrySet().forEach(entry -> entry.setValue(false));
    }

    public double getAveragePutTimeInMS() {
        return timeSpentOnPut.stream().mapToDouble(a -> a).average().orElse(0);
    }

    public int getEvictionsCount() {
        return evictionsCount;
    }

    public synchronized void cleanUp() {
        cache.clear();
    }
}
