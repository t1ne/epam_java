package com.epam.algorithm.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.CacheStats;
import com.google.common.cache.LoadingCache;
import com.google.common.cache.RemovalListener;
import lombok.extern.java.Log;
import java.util.concurrent.TimeUnit;

@Log
public class GuavaCacheService implements CacheService{
    private final LoadingCache<String, CacheEntry> cache;
    public GuavaCacheService() {
        CacheLoader<String, CacheEntry> loader;
        loader = new CacheLoader<>() {
            @Override
            public CacheEntry load(String key) {
                return new CacheEntry(key);
            }
        };

        RemovalListener<String, CacheEntry> listener = n -> {
            if (n.wasEvicted()) {
                log.info(n.getKey() + " was evicted, cause: " + n.getCause().name());
            }
        };

        cache = CacheBuilder.newBuilder()
                .maximumSize(100000)
                .expireAfterAccess(5, TimeUnit.SECONDS)
                .removalListener(listener)
                .recordStats()
                .build(loader);
    }

    @Override
    public CacheEntry get(String key) {
        return cache.getUnchecked(key);
    }

    @Override
    public void put(CacheEntry entry) {
        cache.put(entry.getValue(), entry);
    }

    public CacheStats getStats() {
        return cache.stats();
    }

    public void cleanUp() {
        cache.invalidateAll();
        cache.cleanUp();
    }
}
