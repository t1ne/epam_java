package com.epam.algorithm;

import java.util.List;

public interface SortingStrategy {
    /**
     * Sorts input list in ascending order
     * @param list input list which needs to be sorted
     * @return sorted list in ascending order
     */
    List<Integer> sort(List<Integer> list);
}
