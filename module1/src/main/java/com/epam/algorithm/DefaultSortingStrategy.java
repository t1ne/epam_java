package com.epam.algorithm;

import java.util.Collections;
import java.util.List;

public class DefaultSortingStrategy implements SortingStrategy {
    /**
     * {@inheritDoc}
     */
    @Override
    public List<Integer> sort(List<Integer> list) {
        Collections.sort(list);
        return list;
    }
}
