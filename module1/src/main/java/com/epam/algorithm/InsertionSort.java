package com.epam.algorithm;

import java.util.Arrays;
import java.util.List;

public class InsertionSort implements SortingStrategy {
    /**
     * {@inheritDoc}
     */
    @Override
    public List<Integer> sort(List<Integer> list) {
        Integer[] arr = list.toArray(new Integer[0]);
        insertionSort(arr);
        return Arrays.asList(arr);
    }

    private void insertionSort(Integer[] list) {
        for (int rightIndex = 1; rightIndex < list.length; rightIndex++) {
            int elementToBeSorted = list[rightIndex];
            int leftIndex = rightIndex - 1;
            while (leftIndex >= 0 && list[leftIndex] > elementToBeSorted) {
                list[leftIndex + 1] = list[leftIndex];
                leftIndex = leftIndex - 1;
            }
            list[leftIndex + 1] = elementToBeSorted;
        }
    }
}
