package com.epam.algorithm;

import java.util.List;

public class RecursiveBinarySearch extends BinarySearch {
    /**
     * {@inheritDoc}
     */
    @Override
    public int search(List<Integer> array, Integer searchedValue) {
        List<Integer> sortedArray = sortingStrategy.sort(array);
        return binarySearch(sortedArray, searchedValue, 0, array.size() - 1);
    }

    private int binarySearch(List<Integer> arr, int searchedValue, int leftBound, int rightBound) {
        int middle = (leftBound + rightBound)/2;
        if(rightBound < leftBound){
            return -1;
        }
        if (searchedValue < arr.get(middle)){
            return binarySearch(arr, searchedValue, leftBound, middle - 1);
        }
        if (searchedValue > arr.get(middle)){
            return binarySearch(arr, searchedValue, middle + 1, rightBound);
        }
        if (searchedValue == arr.get(middle)){
            return middle;
        }
        return -1;
    }
}
