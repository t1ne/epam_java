package com.epam.algorithm;

import java.util.List;

public abstract class BinarySearch {
    protected SortingStrategy sortingStrategy = new DefaultSortingStrategy();
    /**
     * @param sortingStrategy strategy which will be used to sort input array before performing search
     */
    public void setSortingStrategy(SortingStrategy sortingStrategy) {
        this.sortingStrategy = sortingStrategy;
    }

    /**
     * Sorts input array and searched for index of given element
     * @param array input array, can be unsorted
     * @param searchedValue value which is searched
     * @return index of searched value; -1 if value is not present
     */
    abstract public int search(List<Integer> array, Integer searchedValue);
}
