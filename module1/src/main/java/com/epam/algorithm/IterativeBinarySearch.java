package com.epam.algorithm;

import java.util.List;

public class IterativeBinarySearch extends BinarySearch {
    /**
     * {@inheritDoc}
     */
    @Override
    public int search(List<Integer> array, Integer searchedValue) {
        List<Integer> sortedArray = sortingStrategy.sort(array);
        return binarySearch(sortedArray, searchedValue);
    }

    private int binarySearch(List<Integer> arr, int x) {
        int start = 0;
        int end = arr.size() - 1;
        while (start <= end) {
            int middle = (start + end) / 2;

            if (x < arr.get(middle)) {
                end = middle - 1;
            }

            if (x > arr.get(middle)) {
                start = middle + 1;
            }

            if (x == arr.get(middle)) {
                return middle;
            }
        }
        return -1;
    }
}
