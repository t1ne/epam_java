```
Benchmark                                                    (arraySize)  Mode  Cnt        Score        Error  Units
SearchBenchmarkTest.benchmark_array_binary_search_iterative           10  avgt   10        0.270 ±      0.008  us/op
SearchBenchmarkTest.benchmark_array_binary_search_iterative           50  avgt   10        1.859 ±      0.189  us/op
SearchBenchmarkTest.benchmark_array_binary_search_iterative          100  avgt   10        3.765 ±      0.181  us/op
SearchBenchmarkTest.benchmark_array_binary_search_iterative          500  avgt   10       33.558 ±      0.952  us/op
SearchBenchmarkTest.benchmark_array_binary_search_iterative         1000  avgt   10       95.322 ±      5.009  us/op
SearchBenchmarkTest.benchmark_array_binary_search_iterative        10000  avgt   10     1270.784 ±     42.942  us/op
SearchBenchmarkTest.benchmark_array_binary_search_iterative       100000  avgt   10    14239.088 ±    518.977  us/op
SearchBenchmarkTest.benchmark_array_binary_search_recursive           10  avgt   10        0.262 ±      0.011  us/op
SearchBenchmarkTest.benchmark_array_binary_search_recursive           50  avgt   10        1.599 ±      0.108  us/op
SearchBenchmarkTest.benchmark_array_binary_search_recursive          100  avgt   10        3.944 ±      0.335  us/op
SearchBenchmarkTest.benchmark_array_binary_search_recursive          500  avgt   10       32.329 ±      2.785  us/op
SearchBenchmarkTest.benchmark_array_binary_search_recursive         1000  avgt   10       95.659 ±      2.430  us/op
SearchBenchmarkTest.benchmark_array_binary_search_recursive        10000  avgt   10     1263.353 ±     47.485  us/op
SearchBenchmarkTest.benchmark_array_binary_search_recursive       100000  avgt   10    14159.183 ±    246.434  us/op
SortBenchmarkTest.benchmark_array_insertion_sort                      10  avgt   10        0.131 ±      0.001  us/op
SortBenchmarkTest.benchmark_array_insertion_sort                      50  avgt   10        1.424 ±      0.034  us/op
SortBenchmarkTest.benchmark_array_insertion_sort                     100  avgt   10        4.612 ±      0.198  us/op
SortBenchmarkTest.benchmark_array_insertion_sort                     500  avgt   10       97.655 ±     16.362  us/op
SortBenchmarkTest.benchmark_array_insertion_sort                    1000  avgt   10      330.976 ±      0.720  us/op
SortBenchmarkTest.benchmark_array_insertion_sort                   10000  avgt   10    36573.033 ±    893.403  us/op
SortBenchmarkTest.benchmark_array_insertion_sort                  100000  avgt   10  5623842.940 ± 701298.117  us/op
SortBenchmarkTest.benchmark_array_merge_sort                          10  avgt   10        1.076 ±      0.021  us/op
SortBenchmarkTest.benchmark_array_merge_sort                          50  avgt   10        7.845 ±      0.216  us/op
SortBenchmarkTest.benchmark_array_merge_sort                         100  avgt   10       17.744 ±      0.438  us/op
SortBenchmarkTest.benchmark_array_merge_sort                         500  avgt   10      102.500 ±      2.557  us/op
SortBenchmarkTest.benchmark_array_merge_sort                        1000  avgt   10      260.825 ±      3.430  us/op
SortBenchmarkTest.benchmark_array_merge_sort                       10000  avgt   10     2770.470 ±     72.990  us/op
SortBenchmarkTest.benchmark_array_merge_sort                      100000  avgt   10    34246.886 ±    406.805  us/op
```
From the benchmark test result it is clear that both binary search implementation show same efficiency no matter what 
input array size is.<br> Situation with merge and insertion sort is slightly different, insertion is better suitable for
arrays with size less than 500 elements, and anything bigger than that is a perfect candidate to be sorted with merge 
sort algorithm, as it shows better performance on huge chunks of data.