import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

public class Main {
    private static final Random random = new Random();
    public static void main(String[] args) {
//        defaultMapImplementation();
//        concurrentMapImplementation();
//        synchronizedMapImplementation();
        customThreadSafedMapImplementation();
    }

    private static void defaultMapImplementation() {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        runThreads(hashMap);
    }

    private static void concurrentMapImplementation() {
        Map<Integer, Integer> hashMap = new ConcurrentHashMap<>();
        runThreads(hashMap);
    }

    private static void synchronizedMapImplementation() {
        Map<Integer, Integer> hashMap = Collections.synchronizedMap(new HashMap<>());
        runThreads(hashMap);
    }

    private static void customThreadSafedMapImplementation() {
        Map<Integer, Integer> hashMap = new ThreadSafeMap<>();
        runThreads(hashMap);
    }

    private static void runThreads(Map<Integer, Integer> hashMap) {
        new Thread(() -> {
            while (!(Thread.currentThread().isInterrupted())) {
                try {
                    Integer integer = random.nextInt(10000);
                    hashMap.put(integer, integer);
                    System.out.println("Put " + integer + " into map");
                    Thread.sleep(40);
                } catch (InterruptedException | ConcurrentModificationException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        }).start();
        new Thread(() -> {
            while (!(Thread.currentThread().isInterrupted())) {
                try {
                    Optional<Integer> sumOpt;
                    //  using synchronized block as synchronizedMap implementation don`t
                    //  provide synchronized access for iteration over map elements
                    synchronized (hashMap) {
                        sumOpt = hashMap.values().stream().reduce(Integer::sum);
                    }
                    System.out.println("Sum of elements in the map: " + sumOpt.orElse(0));
                    Thread.sleep(50);
                } catch (InterruptedException | ConcurrentModificationException e) {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
            }
        }).start();
    }
}
