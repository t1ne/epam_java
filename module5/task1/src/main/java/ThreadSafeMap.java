import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ThreadSafeMap<K, V> implements Map<K, V> {
    private final List<Entry<K, V>> entryList = new LinkedList<>();
    private final Lock lock = new ReentrantLock();

    @Override
    public int size() {
        lock.lock();
        int size = entryList.size();
        lock.unlock();
        return size;
    }

    @Override
    public boolean isEmpty() {
        lock.lock();
        boolean isEmpty = entryList.isEmpty();
        lock.unlock();
        return isEmpty;
    }

    @Override
    public boolean containsKey(Object key) {
        lock.lock();
        boolean isPresent = entryList
                .stream()
                .anyMatch(entry -> entry.getKey().equals(key));
        lock.unlock();
        return isPresent;
    }

    @Override
    public boolean containsValue(Object value) {
        lock.lock();
        boolean isPresent =  entryList
                .stream()
                .anyMatch(entry -> entry.getValue().equals(value));
        lock.unlock();
        return isPresent;
    }

    @Override
    public V get(Object key) {
        lock.lock();
        V value =  entryList
                .stream()
                .filter(entry -> entry.getKey().equals(key))
                .findFirst()
                .orElse(new MapEntry<>(null, null))
                .getValue();
        lock.unlock();
        return value;
    }

    public V put(K key, V value) {
        MapEntry<K, V> newEntry = new MapEntry<>(key, value);
        lock.lock();
        if (containsKey(key)) {
            remove(key);
        }
        entryList.add(newEntry);
        lock.unlock();
        return value;
    }

    @Override
    public V remove(Object key) {
        OptionalInt indexOpt = IntStream.range(0, entryList.size())
                .filter(index -> key.equals(entryList.get(index).getKey()))
                .findFirst();
        if (indexOpt.isPresent()) {
            return entryList.remove(indexOpt.getAsInt()).getValue();
        } else {
            return null;
        }
    }

    @Override
    public void putAll(Map<? extends K, ? extends V> m) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        lock.lock();
        entryList.clear();
        lock.unlock();
    }

    @Override
    public Set<K> keySet() {
        lock.lock();
        Set<K> keys = entryList
                .stream()
                .map(Entry::getKey)
                .collect(Collectors.toSet());
        lock.unlock();
        return keys;
    }

    @Override
    public Collection<V> values() {
        lock.lock();
        List<V> values = entryList
                .stream()
                .map(Entry::getValue)
                .collect(Collectors.toList());
        lock.unlock();
        return values;
    }

    @Override
    public Set<Entry<K, V>> entrySet() {
        lock.lock();
        Set<Entry<K,V>> entrySet = new HashSet<>(entryList);
        lock.unlock();
        return entrySet;
    }
}
