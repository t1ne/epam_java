public class Consumer implements Runnable {
    private MessageBus messageBus;

    public void setMessageBus(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()) {
            String message = messageBus.getAsync();
            System.out.println("READ: " + message);
        }
    }
}
