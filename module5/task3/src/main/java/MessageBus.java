import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class MessageBus {
    private final Queue<String> messageQueue = new LinkedList<>();
    private final ReadWriteLock readWriteLock = new ReentrantReadWriteLock();

    public void postAsync(String message) {
        readWriteLock.writeLock().lock();
        messageQueue.add(message);
        readWriteLock.writeLock().unlock();
    }

    public String getAsync() {
        readWriteLock.readLock().lock();
        String message = messageQueue.poll();
        readWriteLock.readLock().unlock();
        return message;
    }
}
