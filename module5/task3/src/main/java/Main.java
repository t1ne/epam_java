public class Main {
    public static void main(String[] args) {
        MessageBus messageBus = new MessageBus();

        Producer producer = new Producer();
        producer.setMessageBus(messageBus);

        Consumer consumer = new Consumer();
        consumer.setMessageBus(messageBus);

        new Thread(producer).start();
        new Thread(producer).start();
        new Thread(consumer).start();
    }
}
