import org.apache.commons.lang3.RandomStringUtils;

public class Producer implements Runnable {

    private MessageBus messageBus;

    public void setMessageBus(MessageBus messageBus) {
        this.messageBus = messageBus;
    }

    @Override
    public void run() {
        while(!Thread.currentThread().isInterrupted()) {
            String message = getRandomMessage();
            messageBus.postAsync(message);
            System.out.println("WROTE: " + message);
        }
    }

    private String getRandomMessage() {
        return RandomStringUtils.randomAlphanumeric(10);
    }
}
