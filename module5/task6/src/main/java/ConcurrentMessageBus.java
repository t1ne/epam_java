import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class ConcurrentMessageBus implements MessageBus {
    private final Queue<String> messageQueue = new LinkedBlockingQueue<>();

    public void postAsync(String message) {
        messageQueue.add(message);
    }

    public String getAsync() {
        return messageQueue.poll();
    }
}
