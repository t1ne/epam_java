public interface MessageBus {

    void postAsync(String message);

    String getAsync();
}
