import java.util.LinkedList;
import java.util.Queue;

public class ClassicMessageBus implements MessageBus {
    private final Queue<String> messageQueue = new LinkedList<>();
    private final Object readLock = new Object();
    private final Object writeLock = new Object();

    public void postAsync(String message) {
        synchronized (writeLock) {
            messageQueue.add(message);
        }
    }

    public String getAsync() {
        synchronized (readLock) {
            return messageQueue.poll();
        }
    }
}
