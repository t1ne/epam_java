public class Main {
    public static void main(String[] args) {
//        classicImplementation();
        concurrentImplementation();
    }

    private static void classicImplementation() {
        MessageBus messageBus = new ClassicMessageBus();
        startThreads(messageBus);
    }

    private static void concurrentImplementation() {
        MessageBus messageBus = new ConcurrentMessageBus();
        startThreads(messageBus);
    }

    private static void startThreads(MessageBus messageBus) {
        Producer producer = new Producer();
        producer.setMessageBus(messageBus);

        Consumer consumer = new Consumer();
        consumer.setMessageBus(messageBus);

        new Thread(producer).start();
        new Thread(producer).start();
        new Thread(consumer).start();
    }
}
