import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Main {
    private static final List<Integer> collection = new LinkedList<>();
    private static final Random random = new Random();

    public static void main(String[] args) {
        new Thread(firstThreadTask()).start();
        new Thread(secondThreadTask()).start();
        new Thread(thirdThreadTask()).start();
    }

    private static Runnable firstThreadTask() {
        return () -> {
            while (!Thread.currentThread().isInterrupted()) {
                synchronized (Main.class) {
                    Integer randomValue = random.nextInt(100);
                    System.out.println("PUT: " + randomValue);
                    collection.add(randomValue);
                }
            }
        };
    }

    private static Runnable secondThreadTask() {
        return () -> {
            while (!Thread.currentThread().isInterrupted()) {
                synchronized (Main.class) {
                    System.out.println("SUM: " +
                            collection.stream()
                                    .reduce(Integer::sum)
                                    .get());
                }
            }
        };
    }

    private static Runnable thirdThreadTask() {
        return () -> {
            while (!Thread.currentThread().isInterrupted()) {
                synchronized (Main.class) {
                    System.out.println("SQRT OF SQUARES SUM: " +
                            Math.sqrt(collection
                                    .stream()
                                    .map(value -> value * value)
                                    .reduce(Integer::sum)
                                    .get()));
                }

            }
        };
    }
}
