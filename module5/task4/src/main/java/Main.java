public class Main {
    public static void main(String[] args) {
        BlockingObjectPool objectPool = new BlockingObjectPool(10);

        for (int i = 0; i < 20; i++) {
            new Thread(() -> {
                while (!Thread.currentThread().isInterrupted()) {
                    try {
                        Object newObject = new Object();
                        objectPool.take(new Object());
                        System.out.println("Put: " + newObject);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }

        new Thread(() -> {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    System.out.println("Got: " + objectPool.get());
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
