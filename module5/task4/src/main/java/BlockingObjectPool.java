import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Pool that block when it has not any items or it full
 */
public class BlockingObjectPool {

    private final BlockingQueue<Object> blockingQueue;

    /**
     * Creates filled pool of passed size
     *
     * @param size of pool
     */
    public BlockingObjectPool(int size) {
        blockingQueue = new LinkedBlockingQueue<>(size);
    }

    /**
     * Gets object from pool or blocks if pool is empty
     *
     * @return object from pool
     * @throws InterruptedException if interrupted while waiting
     */
    public Object get() throws InterruptedException {
        return blockingQueue.take();
    }

    /**
     * Puts object to pool or blocks if pool is full
     *
     * @param object to be taken back to pool
     * @throws InterruptedException if interrupted while waiting
     */
    public void take(Object object) throws InterruptedException {
        blockingQueue.put(object);
    }
}