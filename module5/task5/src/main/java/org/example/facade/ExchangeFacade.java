package org.example.facade;

import lombok.extern.slf4j.Slf4j;
import org.example.exception.NotEnoughFundsException;
import org.example.model.Currency;
import org.example.model.ExchangeRates;
import org.example.model.UserAccount;

import java.math.BigDecimal;

@Slf4j
public class ExchangeFacade {

    public synchronized void exchangeInsideAccount(UserAccount account,
                                                   Currency from,
                                                   Currency to,
                                                   BigDecimal amount) {
        transferBetweenAccounts(account, from, account, to, amount);
    }

    public synchronized void transferBetweenAccounts(UserAccount sourceUserAccount,
                                        Currency from,
                                        UserAccount destinationUserAccount,
                                        Currency to,
                                        BigDecimal amount) {
        if (sourceUserAccount.getBalance(from).compareTo(amount) < 0) {
            throw new NotEnoughFundsException(
                    sourceUserAccount.getUsername() + " - " +
                            sourceUserAccount.getBalance(from) + " " + from + " left");
        }
        BigDecimal exchangeRate = ExchangeRates.getInstance().getExchangeRate(from, to);
        log.info("Received transfer request: " +
                "from " + sourceUserAccount.getUsername() + "; to " + destinationUserAccount.getUsername() + "; " +
                amount + " " + from + " -> " + to);
        sourceUserAccount.withdraw(from, amount);
        destinationUserAccount.topUpBalance(to, amount.multiply(exchangeRate));
    }
}
