package org.example.model;

import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class UserAccount {
    private final String username;
    private final Map<Currency, BigDecimal> moneyAmount;

    public UserAccount(String username) {
        this.username = username;
        this.moneyAmount = new HashMap<>(Map.of(
                Currency.UAH, BigDecimal.ZERO,
                Currency.GBP, BigDecimal.ZERO,
                Currency.HUF, BigDecimal.ZERO,
                Currency.EUR, BigDecimal.ZERO,
                Currency.USD, BigDecimal.ZERO));
    }

    public String getUsername() {
        return username;
    }

    public BigDecimal getBalance(Currency currency) {
        return moneyAmount.get(currency);
    }

    public BigDecimal withdraw(Currency currency, BigDecimal amount) {
        amount = amount.setScale(2, RoundingMode.HALF_UP);
        BigDecimal currentAmount = moneyAmount.get(currency);
        BigDecimal amountAfterWithdrawal = currentAmount.subtract(amount);
        log.info(username + ": withdrawing " + amount + "  " + currency + ", "
                + amountAfterWithdrawal + " " + currency + " left");
        moneyAmount.put(currency, amountAfterWithdrawal);
        return amountAfterWithdrawal;
    }

    public BigDecimal topUpBalance(Currency currency, BigDecimal amount) {
        amount = amount.setScale(2, RoundingMode.HALF_UP);
        BigDecimal currentAmount = moneyAmount.get(currency);
        BigDecimal amountAfterReplenishment = currentAmount.add(amount);
        log.info(username + ": adding  " + amount + " " + currency + " to the account, current balance is "
                + amountAfterReplenishment + " " + currency);
        moneyAmount.put(currency, amountAfterReplenishment);
        return amountAfterReplenishment;
    }
}

