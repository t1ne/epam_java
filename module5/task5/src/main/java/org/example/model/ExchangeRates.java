package org.example.model;

import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import org.apache.commons.lang3.tuple.Pair;
import org.example.exception.CurrencyNotFoundException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ExchangeRates {
    private final Multimap<Currency, Pair<Currency, BigDecimal>> exchangeRate;

    public ExchangeRates() {
        exchangeRate = LinkedListMultimap.create();
        exchangeRate.putAll(Currency.UAH, List.of(
                Pair.of(Currency.USD, new BigDecimal("27.82")),
                Pair.of(Currency.GBP, new BigDecimal("36.927")),
                Pair.of(Currency.HUF, new BigDecimal("8.99")),
                Pair.of(Currency.EUR, new BigDecimal("32.12")))
        );
    }

    public static ExchangeRates getInstance() {
        return new ExchangeRates();
    }

    public Map<Currency, BigDecimal> getExchangeRate(Currency currency) {
        return exchangeRate
                .get(currency)
                .stream()
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
    }

    public BigDecimal getExchangeRate(Currency from, Currency to) {
        return BigDecimal.ONE.divide(exchangeRate
                .get(from)
                .stream()
                .filter(pair -> pair.getKey().equals(to))
                .map(Pair::getValue)
                .findFirst().orElseThrow(CurrencyNotFoundException::new), 8, RoundingMode.HALF_UP);
    }
}
