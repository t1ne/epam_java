package org.example.model;

public enum Currency {
    USD,
    UAH,
    GBP,
    HUF,
    EUR
}
