package org.example;

import org.example.dao.UserAccountDao;
import org.example.facade.ExchangeFacade;
import org.example.model.Currency;
import org.example.model.ExchangeRates;
import org.example.model.UserAccount;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    static Random random = new Random();

    public static void main(String[] args) {
        ExchangeFacade exchangeFacade = new ExchangeFacade();
        ExecutorService executor = Executors.newFixedThreadPool(10);
        List<UserAccount> userAccounts = new UserAccountDao().readAll();
        Runnable runnableTask = () -> {
            UserAccount from = userAccounts.get(random.nextInt(10));
            UserAccount to = userAccounts.get(random.nextInt(10));
            exchangeFacade.transferBetweenAccounts(
                    from, getRandomCurrency(),
                    to, getRandomCurrency(),
                    generateRandomBigDecimalFromRange(BigDecimal.ZERO, BigDecimal.valueOf(10000000000L)));
        };
        while (true) {
            executor.submit(runnableTask);
        }
    }

    private static Currency getRandomCurrency() {
            int pick = random.nextInt(Currency.values().length);
            return Currency.values()[pick];
    }

    public static BigDecimal generateRandomBigDecimalFromRange(BigDecimal min, BigDecimal max) {
        BigDecimal randomBigDecimal = min.add(BigDecimal.valueOf(Math.random()).multiply(max.subtract(min)));
        return randomBigDecimal.setScale(2, RoundingMode.HALF_UP);
    }
}
