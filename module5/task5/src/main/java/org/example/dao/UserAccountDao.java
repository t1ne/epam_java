package org.example.dao;

import org.example.model.Currency;
import org.example.model.UserAccount;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class UserAccountDao {

    public List<UserAccount> readAll() {
        File folder = getAccountsFolder();
        if (folder != null && folder.isDirectory()) {
            List<UserAccount> userAccounts = new LinkedList<>();
            for (final File fileEntry : folder.listFiles()) {
                userAccounts.add(parse(fileEntry));
            }
            return userAccounts;
        } else {
            return Collections.emptyList();
        }
    }

    private File getAccountsFolder() {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource("accounts");
        try {
            if (resource == null) {
                throw new FileNotFoundException();
            }
            return new File(resource.toURI());
        } catch (URISyntaxException | FileNotFoundException uriSyntaxException) {
            System.out.println("Error while parsing accounts folder");
            uriSyntaxException.printStackTrace();
            return null;
        }
    }

    private UserAccount parse(File userAccountFile) {
        UserAccount userAccount = new UserAccount(userAccountFile.getName());
        List<String> userAccountBalances = readFileContent(userAccountFile);
        for (String userAccountBalance: userAccountBalances) {
            String[] currencyAndBalance = userAccountBalance.split(" ");
            int CURRENCY_INDEX = 0;
            int BALANCE_INDEX = 1;
            Currency currency = Currency.valueOf(currencyAndBalance[CURRENCY_INDEX]);
            BigDecimal balance = new BigDecimal(currencyAndBalance[BALANCE_INDEX]);
            userAccount.topUpBalance(currency, balance);
        }
        return userAccount;
    }

    private List<String> readFileContent(File fileName) {
       List<String> stringList = new LinkedList<>();
        try (InputStream inputStream = new FileInputStream(fileName)) {
            BufferedReader br
                    = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = br.readLine()) != null) {
                stringList.add(line);
            }
        } catch (IOException e) {
            System.out.println("Error while reading account file");
            e.printStackTrace();
        }
        return stringList;
    }

    public void write(UserAccount userAccount) {

    }
}
